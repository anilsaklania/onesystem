using MediatR;
using MediatR.Pipeline;
using Microsoft.EntityFrameworkCore;
using System.Reflection;
using Microsoft.AspNetCore.ResponseCompression;
using System.IO.Compression;
using OneSystem.Application.Products.Queries.GetProductAttributesSearch;
using Microsoft.OData.Edm;
using Microsoft.Net.Http.Headers;
using Microsoft.AspNetCore.OData;
using Microsoft.OData.ModelBuilder;
using Microsoft.AspNetCore.OData.Formatter;
using OneSystem.Persistence.Entities;
using OneSystem.Domain.Entities;

int[] sqlRetryList = new int[] { -1, 701, 1222, 8645, 8651, -2, 1204, 1205, 30053 };
var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

//builder.Services.AddControllersWithViews();


builder.Services.AddDbContext<OneSystemDbContext>(options =>
             options.UseSqlServer(builder.Configuration.GetConnectionString("OneSystemDbConnectionString"), providerOptions => providerOptions.EnableRetryOnFailure(maxRetryCount: 25, maxRetryDelay: TimeSpan.FromSeconds(30), sqlRetryList)));

//builder.Services.AddOData();


builder.Services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
// mediatr services 
builder.Services.AddScoped(typeof(IPipelineBehavior<,>), typeof(RequestPreProcessorBehavior<,>));
builder.Services.AddScoped(typeof(IPipelineBehavior<,>), typeof(RequestPostProcessorBehavior<,>));
builder.Services.AddMediatR(typeof(GetProductAttributesSearchQueryHandler).GetTypeInfo().Assembly);

//json self referencing  
builder.Services.AddControllers().AddOData(opt =>
    opt.Select().Expand().Filter().OrderBy().SetMaxTop(null).Count().AddRouteComponents("odata", GetEdmModel())
);

builder.Services.AddControllers().AddNewtonsoftJson(options =>
{
    options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
    options.SerializerSettings.NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore;
    options.SerializerSettings.ContractResolver = new Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver();
});


//builder.Services.AddSwaggerGen(c =>
//{
//    c.SwaggerDoc("v1", new OpenApiInfo { Title = "My API", Version = "v1" });   
//});
builder.Services.AddCors(c =>
{
    c.AddPolicy("AllowOrigin", options => options.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader());
});

//var mappingConfig = new MapperConfiguration(mc =>
//{
//    mc.AddProfile(new ProductMappingProfile());
//});

//IMapper mapper = mappingConfig.CreateMapper();
//builder.Services.AddSingleton(mapper);


builder.Services.Configure<GzipCompressionProviderOptions>(options => options.Level = CompressionLevel.Fastest);

// Add Response compression services
builder.Services.AddResponseCompression(options =>
{
    options.Providers.Add<GzipCompressionProvider>();
});
AddOdataFormatters(builder.Services);

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseExceptionHandler("/Error");
    app.UseHsts();
}
else
{
    app.UseDeveloperExceptionPage();
}

app.UseAuthentication();
app.UseRouting();
app.UseAuthorization();


app.UseEndpoints(endpoints =>
{
    endpoints.MapControllers();
});
app.UseStaticFiles();
app.UseResponseCompression();
app.UseCors(options => options.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader());
app.UseSwagger();

//app.UseSwaggerUI(c =>
//{
//    c.SwaggerEndpoint("/swagger/v1/swagger.json", "OneSystem Site API");    
//});

app.MapControllerRoute(
    name: "default",
    pattern: "{controller}/{action=Index}/{id?}");

app.MapFallbackToFile("index.html");

app.Run();



static IEdmModel GetEdmModel()
{
    var edmBuilder = new ODataConventionModelBuilder();
    edmBuilder.EntitySet<ProductAttribute>("ProductAttributes");    
    return edmBuilder.GetEdmModel();
}
static void AddOdataFormatters(IServiceCollection services)
{
    services.AddMvcCore(options =>
    {
        foreach (var outputFormatter in options.OutputFormatters.OfType<ODataOutputFormatter>().Where(_ => _.SupportedMediaTypes.Count == 0))
            outputFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("applciation/prs.odatatestxx-odata"));
        foreach (var inputFormatter in options.InputFormatters.OfType<ODataInputFormatter>().Where(_ => _.SupportedMediaTypes.Count == 0))
            inputFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("applciation/prs.odatatestxx-odata"));
    });
}