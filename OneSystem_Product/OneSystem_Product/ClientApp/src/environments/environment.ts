// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  stsAuthority: 'http://localhost:5003',
  clientId: 'Main Site',
  clientRoot: 'http://localhost:5096',
  clientScope: 'openid profile main_site',
  apiRoot: 'http://localhost:11314/api',
  promostandardsRoot: 'http://localhost:1299', 
};


/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
