export const environment = {
  production: true,
  stsAuthority: 'http://identity.onesystem.positive.local',
  clientId: 'Main Site',
  clientRoot: 'http://onesystem.positive.local',
  clientScope: 'openid profile main_site',
  apiRoot: 'http://api.onesystem.positive.local/api',
  promostandardsRoot: 'http://promostandards.onesystem.positive.local'
};
