import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { state, style, transition, animate, trigger } from '@angular/animations';
import { Title } from '@angular/platform-browser';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { DisplayService } from './services/shared/display.service';
import { MessageService, PrimeNGConfig } from 'primeng-lts/api';
import { Toast } from 'primeng-lts/toast';
import { TreeNode } from 'primeng-lts/api';
import { OverlayPanel } from 'primeng-lts/overlaypanel';
import * as moment from 'moment';

import { OdataService } from './services/odata.service';

declare var $: any;
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  animations: [
    trigger('breathe', [
      state('in', style({
        'font-size': '65px',
        'margin-left': '100px',
        'margin-top': '0px'
      })),
      state('out', style({
        'font-size': '45px',
        'margin-left': '-120px',
        'margin-top': '40px'
      })),
      transition('in <=> out', [
        animate('8s linear')
      ])])
  ]
})
export class AppComponent implements OnInit, AfterViewInit {
  quickNavDisplay: boolean = false;
  navState: boolean = true;
  userSettingState: boolean = false;
  currentQueue: any[] = [];
  currentNavigation: any[];
  queueInterval: any; 
  user: any;
  showQueueDetails: boolean = false;
  @ViewChild('op') overlay: OverlayPanel;
  @ViewChild('queueToast') queueToast: Toast;
  constructor(private titleService: Title,
    private router: Router, private activedRoute: ActivatedRoute,
    private displayService: DisplayService, private _message: MessageService, private odata: OdataService,
    private primengConfig: PrimeNGConfig) {
   
    this.displayService.refresh();    
    this.displayService.forceNavMinify.subscribe(x => {
      if (x != this.isMinified)
        this.toggleMinified();
    }); 
  }
  state = 'in';
  ngAfterViewInit() {
    //this.displayService.refresh();
    //setTimeout(() => {
    //  this.state = 'out';
    //}, 0);
  }
  onEnd(event) {
    //this.state = 'in';
    //if (event.toState === 'in') {
    //  setTimeout(() => {
    //    this.state = 'out';
    //  }, 0);
    //}
  }
  ngOnInit(): void {
    this.primengConfig.ripple = true;
    this.router.events.subscribe((val) => {
      if (val instanceof NavigationEnd) {
        this.titleService.setTitle(this.activedRoute.root.firstChild.snapshot.data['title']);
        window.scroll(0, 0);
      }
    });
    //setInterval(() => {
    //  if (this.currentQueue) {
    //    this._message.clear();
    //    this.currentQueue.forEach(x => {
    //      let tmpSources = x.queues.map(x => x.source);
    //      this._message.add({
    //        sticky: true,
    //        closable: true,
    //        severity: "info",
    //        key: "queue",
    //        detail: x.queues.map(x => x.response).join(","),
    //        summary: `Queued Action Completed - ${tmpSources.filter((x, i, a) => x && a.indexOf(x) === i).join(",")}`,
    //        data: x.id
    //      });
    //    });
    //  }
    //}, 10000);
  }

  //navigation code
  isMinified: boolean = false;
  navIndexSelected = -1;
  //navParentIndexSelected = 1;
  //navChildIndexSelected = 1;
  //showSearchBar = false;

  public toggleNavMenuParentSelected(index) {
    if (this.navIndexSelected == index) {
      $('[data-nav-target="' + this.navIndexSelected + '"]').removeClass('open');
      $('[data-nav-target="' + this.navIndexSelected + '"] > a > b > em').removeClass('fa-minus-square-o');
      $('[data-nav-target="' + this.navIndexSelected + '"] > a > b > em').addClass('fa-plus-square-o');
      this.navIndexSelected = -1;
    } else {
      $('[data-nav-target="' + this.navIndexSelected + '"]').removeClass('open');
      $('[data-nav-target="' + this.navIndexSelected + '"] > a > b > em').removeClass('fa-minus-square-o');
      $('[data-nav-target="' + this.navIndexSelected + '"] > a > b > em').addClass('fa-plus-square-o');
      this.navIndexSelected = index;
      $('[data-nav-target="' + index + '"] > a > b > em').removeClass('fa-plus-square-o');
      $('[data-nav-target="' + index + '"]').addClass('open');
      $('[data-nav-target="' + index + '"] > a > b > em').addClass('fa-minus-square-o');
    }
  }

  public toggleInnerMenu(parentIndex, childIndex, event) {
    $('[data-nav-target="' + parentIndex + '"]').find('[data-nav-inner="' + childIndex + '"]').toggle();
    var target = event.target || event.srcElement || event.currentTarget;
    var toggleIcon = $(target).find('em');
    var isOpen = toggleIcon.hasClass('fa-minus-square-o');
    if (isOpen) {
      toggleIcon.removeClass('fa-minus-square-o');
      toggleIcon.addClass('fa-plus-square-o');
    } else {
      toggleIcon.removeClass('fa-plus-square-o');
      toggleIcon.addClass('fa-minus-square-o');
    }
  }
  public toggleMinified() {
    if ($('body').hasClass('minified')) {
      $('body').removeClass('minified');
      this.isMinified = false;
    } else {
      $('body').addClass('minified');
      this.isMinified = true;
    }
  }
  public navigationClicked(nav) {
    if (nav.name == "System Queue") {
      this.showQueueDetails = true;
      
    }
  }
  recentSystemQueues: TreeNode[];
  currentMeta: string;
  showOverlay(meta, evt) {
    this.currentMeta = meta;
    this.overlay.show(evt);
  }

  navigationClickedDetailed(nav) {

  }

  onToastClose(evt) {
    let msg = evt.message;    
    this.currentQueue = this.currentQueue.filter(x => x.id != msg.data);
  }


  onQueueRefreshClicked(evt) {  
  }
}
