import { Injectable } from '@angular/core';
import {
  HttpClient, HttpHeaders, HttpParams,
  HttpResponse, HttpEvent
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment'; 
@Injectable()
export class OdataService {
  public accessToken: string;
  private headers = new HttpHeaders();
  private httpHeaderAccepts: string[] = [
    'text/plain',
    'application/json',
    'text/json'
  ];
  constructor(protected httpClient: HttpClient) {
  } 
  public query(path: string,odataQuery: string): Observable<any> {
    this.validate();
    return this.httpClient.request<any[]>('get', `${environment.clientRoot}/odata/${path}/?${odataQuery}`,
      {
        headers: this.headers
      }
    );
  }
  private validate() {
    if (this.accessToken)
      this.headers = this.headers.set('Authorization', 'Bearer ' + this.accessToken);
    this.headers = this.headers.set('Accept', this.httpHeaderAccepts);
  }

}
