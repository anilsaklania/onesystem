import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
 
import { MessageService, Message } from 'primeng-lts/api';

@Injectable()
export class NotificationService {
  //private notifications = new Subject<Message>();
  private lastNotice: Message;
  private lastMessageTime: Date;
  //notificationAnnounced$ = this.notifications.asObservable();
  constructor(private messageService: MessageService) { }

  requestNotice(notice: Message) {
    this.messageService.add(notice);
  }

  requestNoticeRestricted(notice: Message) {
    if (this.lastNotice && this.lastNotice.detail == notice.detail && (((new Date()).valueOf() - (this.lastMessageTime).valueOf()) / 1000) < 10)
      return;
    this.messageService.add(notice);
    this.lastNotice = notice;
    this.lastMessageTime = new Date();

  }

}
