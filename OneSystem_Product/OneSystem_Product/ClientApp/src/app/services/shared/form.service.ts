import { Injectable } from '@angular/core';
import { isNullOrUndefined } from 'is-what';
import { Product, ProductPrice, ProductProductPrice, UpdateProductCommand } from '../typescript-angular-client';
import { UtilitiesService } from './utilities-service';
@Injectable()
export class FormService {
  constructor(private utils: UtilitiesService) {

  }

  //hard code defaults 
  defaultFields = ['childAttributeId', 'name', 'value', 'tiedAttributeValue', 'disableProp'];
  defaultPricingFields = ['id', 'min', 'max', 'salePriceExpire'];
  defaultSizeAttributeId = "240237";
  defaultColorAttributeId = "277483";
  convertToUpdateProductModel(creationForm: any, size = null, color = null): UpdateProductCommand {
    let prod: UpdateProductCommand = {
      sku: creationForm.sku,
      attributes: [],
      type: "updateOrCreate",
      copies : []

    };
    //general 
    let generalKeys = this.utils.objectKey(creationForm.general);
    let specKeys = this.utils.objectKey(creationForm.specifications);
    let optionKeys = this.utils.objectKey(creationForm.choices);
    (<any[]>creationForm.components).forEach(x => {
      if (x.sku && x.sku.length > 0 && x.quantity && x.quantity > 0 && x.type) {
        prod.attributes.push({
          parentAttributeId: 240241,
          name: x.type == "PK" ? "PACKAGING" : "PRODUCT COMPONENTS",
          value: `${("" + x.quantity).padStart(3, '0')} ${x.sku} ${x.name}`,
          type: x.type
        });
      }
    });
    if (creationForm.longCopy && creationForm.longCopy.length > 0)
      prod.copies.push({
        type: "AS400",
        name : "Long Description",
        value: creationForm.longCopy
      });
    if (creationForm.shortCopy && creationForm.shortCopy.length > 0)
      prod.copies.push({
        type: "AS400",
        name: "Short Description",
        value: creationForm.shortCopy
      });
    if (creationForm.addAJ)
      prod.attributes.push({
        parentAttributeId: 240241,
        name: "Additional Item/Load T-options",
        value: "",
        type: "AJ"
      });
    if (creationForm.addIG)
      prod.attributes.push({
        parentAttributeId: 240241,
        name: "AS/400 Ignores T-Options",
        value: "",
        type: "IG"
      });
    //general AS400
    generalKeys.filter(x => x.startsWith('as400')).forEach(x => {
      let cur = creationForm.general[x];
      if (cur.nameValueCol[0].value && cur.nameValueCol[0].value.length > 0)
        prod.attributes.push({
          parentAttributeId: cur.attributeId,
          name: cur.nameValueCol[0].name,
          value: cur.nameValueCol[0].value,
          type: cur.nameValueCol[0].tiedAttributeValue
        });
    });
    specKeys.forEach(x => {
      let cur = creationForm.specifications[x];
      (<any[]>cur.nameValueCol).forEach(d => {
        if (d.name && d.name.length > 0) {
          let fields = this.utils.objectKey(d);
          //get non standards
          fields = fields.filter(d => this.defaultFields.indexOf(d) == -1);
          if (fields.length > 0) {
            let good = false;
            fields.forEach(p => {
              if (d[p] && (<string>d[p]).trim().length > 0) {
                prod.attributes.push({
                  parentAttributeId: +x,
                  name: d.name,
                  value: d[p],
                  type: p
                });
                good = true;
              }
            });
            if (!good)
              prod.attributes.push({
                parentAttributeId: +x,
                name: d.name,
                value: !isNullOrUndefined(d.value) && d.value.length > 0 ? d.value : null,
                type: !isNullOrUndefined(d.tiedAttributeValue) && d.tiedAttributeValue.length > 0 ? d.tiedAttributeValue : null
              });
          } else
            prod.attributes.push({
              parentAttributeId: +x,
              name: d.name,
              value: !isNullOrUndefined(d.value) && d.value.length > 0 ? d.value : null,
              type: !isNullOrUndefined(d.tiedAttributeValue) && d.tiedAttributeValue.length > 0 ? d.tiedAttributeValue : null
            });
        }
      });
    });
    optionKeys.forEach(x => {
      let cur = creationForm.choices[x];
      (<any[]>cur.nameValueCol).forEach(d => {
        if (d.name && d.name.length > 0) {
          let fields = this.utils.objectKey(d);
          //get non standards
          fields = fields.filter(d => this.defaultFields.indexOf(d) == -1);
          if (fields.length > 0) {
            let good = false;
            fields.forEach(p => {
              if (d[p] && (<string>d[p]).trim().length > 0) {
                prod.attributes.push({
                  parentAttributeId: +x,
                  name: d.name,
                  value: d[p],
                  type: p
                });
                good = true;
              }
            });
            if (!good)
              prod.attributes.push({
                parentAttributeId: +x,
                name: d.name,
                value: !isNullOrUndefined(d.value) && d.value.length > 0 ? d.value : null,
                type: !isNullOrUndefined(d.tiedAttributeValue) && d.tiedAttributeValue.length > 0 ? d.tiedAttributeValue : null
              });
          } else
            prod.attributes.push({
              parentAttributeId: +x,
              name: d.name,
              value: !isNullOrUndefined(d.value) && d.value.length > 0 ? d.value : null,
              type: !isNullOrUndefined(d.tiedAttributeValue) && d.tiedAttributeValue.length > 0 ? d.tiedAttributeValue : null
            });
        }
      });
    });
    (<any[]>creationForm.notes.nameValueCol).forEach(x => {
      if (x.value && x.value.length > 0)
        (<any[]>x.value).forEach(d => {
          prod.attributes.push({
            parentAttributeId: creationForm.notes.attributeId,
            name: x.name,
            value: d,
            type: !isNullOrUndefined(x.tiedAttributeValue) && x.tiedAttributeValue.length > 0 ? x.tiedAttributeValue : null
          });
        });
    });
    if ((<any[]>creationForm.prices) && (<any[]>creationForm.prices).length > 0) {
      prod.prices = [];
      (<any[]>creationForm.prices).forEach(x => {
        let fields = this.utils.objectKey(x);
        fields.filter(d => this.defaultPricingFields.indexOf(d) == -1).forEach(d => {
          let pprice: ProductProductPrice = {
            division: "PP"
          };
          let price: ProductPrice = {
            quantity: x.min,
            type: "AA"
          };

          if (d == "salePrice") {
            price.typeMeta = "SALE";
            pprice.expireDate = x["salePriceExpire"];
          } else if (d == "fobCost") {
            price.typeMeta = "Landed Cost";
          } else if (d == "cost") {
            price.typeMeta = "Cost";
          } else if (d.startsWith("setup")) {
            price.type = "SU";
            if (d.endsWith("Cost"))
              price.typeMeta = "Cost";
          }
          price.value = x[d];
          pprice.price = price;
          if (+price.value > 0)
            prod.prices.push(pprice);
        });
      });
    }
    return prod;
  }

}
