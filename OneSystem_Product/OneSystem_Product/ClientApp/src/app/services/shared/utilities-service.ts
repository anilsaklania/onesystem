import { Injectable, EventEmitter } from '@angular/core';
import { Product, ProductAttribute } from '../typescript-angular-client';
//import pako from 'pako';

//extension service for primeng datatable
@Injectable()
export class UtilitiesService {
 /* pako: any = pako;*/
    //interesting code (group by)
    objectKey(obj) {
        return Object.keys(obj);
  }
  groupBy(array: any[], parentProperty:string): any[] { 
      return (array.reduce((prev, now) => {
        if (!prev[now[parentProperty]]) {
          prev[now[parentProperty]] =[]; 
        }
        prev[now[parentProperty]].push(now);
        return prev;
      }, {})) as any[];
  }

    //Product utils 
    groupByParentAttributeId(product: Product = null, attributes: any[] = null): any[] {
        if (product)
            return (product.attributes.reduce((prev, now) => {
                if (!prev[now.attribute.parentAttributeId]) {
                    prev[now.attribute.parentAttributeId] = {};
                    prev[now.attribute.parentAttributeId]['parent'] = now.attribute.parentAttribute;
                    prev[now.attribute.parentAttributeId]['attributes'] = [];
                }
                prev[now.attribute.parentAttributeId]['attributes'].push(now.attribute);
                return prev;
            }, {})) as any[];
        else
            return (attributes.reduce((prev, now) => {
                if (!prev[now.attribute.parentAttributeId]) {
                    prev[now.attribute.parentAttributeId] = {};
                    prev[now.attribute.parentAttributeId]['attributes'] = [];
                }
                prev[now.attribute.parentAttributeId]['attributes'].push(now);
                return prev;
            }, {})) as any[];
    }

    getByParentName(product: Product, name: string) {
        return product.attributes.filter(x => x.attribute != null && x.attribute.parentAttribute != null && x.attribute.parentAttribute.name == name).map(x => x.attribute);
    }
    getByNameValue(product: Product, name: string, parentName: string = null) {
        let attributes: ProductAttribute[] = [];
        if (parentName)
            attributes = this.getByParentName(product, parentName);
        return attributes.filter(x => x.name == name).length > 0 ? attributes.filter(x => x.name == name)[0] : {
            value: "N/A"
        };
    }
  share(input : any): void {
    //this.copyMessage(btoa(this.pako.deflate(JSON.stringify(input), { to: 'string' })));
    alert("Share content copied to clipboard.");
  }  
  fromShare(input: string): any { 
    return "";//JSON.parse(this.pako.inflate(atob(input), { to: 'string' }));
  }
  copyMessage(val: string) {
    let selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = val;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
  } 
}
