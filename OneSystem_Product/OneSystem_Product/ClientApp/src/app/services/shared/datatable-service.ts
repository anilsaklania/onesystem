import { Injectable } from '@angular/core';
import { Table } from 'primeng-lts/table';

//extension service for primeng datatable
@Injectable()
export class DataTableService {

  generateDropdownLabels(data: any[], column: string, includeEmpty=true): any[] {
    let results = data.map(y => {
      if (column.indexOf('.') > -1) {
        column.split('.').forEach(x => {
          if (y)
            y = y[x];
        });
        return {
          label: y,
          value: y
        };
      }else
        return {
          label: y[column],
          value: y[column]
        };
    });
    //de-dupe
    results = results.filter((x, i, a) => x.label && a.findIndex(c => c.label == x.label) === i);
    if (includeEmpty)
     results.unshift({ label: '', value: null });
    return results;
  }
  generateDropdownLabelsFromStrings(data: string[]) : any[]{
    let res = []; 
   res = data.map(y => {
      return {
        label: y,
        value: y
      }
    });
    res.unshift({ label: '', value: null });
    return res; 
  }
  scrollTo(table: Table, loc: number) {
    let body = table.containerViewChild.nativeElement.getElementsByClassName("ui-table-scrollable-body")[0];
    body.scrollTop = loc;
  }
 
}
