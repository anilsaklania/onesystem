import { Injectable, HostListener, OnInit, Output, EventEmitter, Directive } from '@angular/core';  

@Directive()
@Injectable()
export class DisplayService {
    innerWidth: number;
    innerHeight: number;

    forceNavMinify: EventEmitter<boolean> = new EventEmitter<boolean>();

    @HostListener('window:resize', ['$event'])
    onResize(event) {
        this.refresh();
  }

    getWidth(percent: number = 0) {
        return this.innerWidth - ((percent / 100) * this.innerWidth);
    }
    getHeight(percent: number = 0) {
        return this.innerHeight - ((percent / 100) * this.innerHeight);
    }
    refresh() {
        this.innerWidth = window.innerWidth;
        this.innerHeight = window.innerHeight;
    }
}
