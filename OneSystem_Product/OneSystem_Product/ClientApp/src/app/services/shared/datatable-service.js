"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DataTableService = void 0;
const core_1 = require("@angular/core");
//extension service for primeng datatable
let DataTableService = class DataTableService {
    generateDropdownLabels(data, column, includeEmpty = true) {
        let results = data.map(y => {
            if (column.indexOf('.') > -1) {
                column.split('.').forEach(x => {
                    if (y)
                        y = y[x];
                });
                return {
                    label: y,
                    value: y
                };
            }
            else
                return {
                    label: y[column],
                    value: y[column]
                };
        });
        //de-dupe
        results = results.filter((x, i, a) => x.label && a.findIndex(c => c.label == x.label) === i);
        if (includeEmpty)
            results.unshift({ label: '', value: null });
        return results;
    }
    generateDropdownLabelsFromStrings(data) {
        let res = [];
        res = data.map(y => {
            return {
                label: y,
                value: y
            };
        });
        res.unshift({ label: '', value: null });
        return res;
    }
    scrollTo(table, loc) {
        let body = table.containerViewChild.nativeElement.getElementsByClassName("ui-table-scrollable-body")[0];
        body.scrollTop = loc;
    }
};
DataTableService = __decorate([
    core_1.Injectable()
], DataTableService);
exports.DataTableService = DataTableService;
//# sourceMappingURL=datatable-service.js.map