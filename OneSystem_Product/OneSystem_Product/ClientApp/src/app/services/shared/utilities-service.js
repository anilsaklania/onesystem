"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UtilitiesService = void 0;
const core_1 = require("@angular/core");
const pako_1 = require("pako");
//extension service for primeng datatable
let UtilitiesService = class UtilitiesService {
    constructor() {
        this.pako = pako_1.default;
    }
    //interesting code (group by)
    objectKey(obj) {
        return Object.keys(obj);
    }
    groupBy(array, parentProperty) {
        return (array.reduce((prev, now) => {
            if (!prev[now[parentProperty]]) {
                prev[now[parentProperty]] = [];
            }
            prev[now[parentProperty]].push(now);
            return prev;
        }, {}));
    }
    //Product utils 
    groupByParentAttributeId(product = null, attributes = null) {
        if (product)
            return (product.attributes.reduce((prev, now) => {
                if (!prev[now.attribute.parentAttributeId]) {
                    prev[now.attribute.parentAttributeId] = {};
                    prev[now.attribute.parentAttributeId]['parent'] = now.attribute.parentAttribute;
                    prev[now.attribute.parentAttributeId]['attributes'] = [];
                }
                prev[now.attribute.parentAttributeId]['attributes'].push(now.attribute);
                return prev;
            }, {}));
        else
            return (attributes.reduce((prev, now) => {
                if (!prev[now.attribute.parentAttributeId]) {
                    prev[now.attribute.parentAttributeId] = {};
                    prev[now.attribute.parentAttributeId]['attributes'] = [];
                }
                prev[now.attribute.parentAttributeId]['attributes'].push(now);
                return prev;
            }, {}));
    }
    getByParentName(product, name) {
        return product.attributes.filter(x => x.attribute != null && x.attribute.parentAttribute != null && x.attribute.parentAttribute.name == name).map(x => x.attribute);
    }
    getByNameValue(product, name, parentName = null) {
        let attributes = [];
        if (parentName)
            attributes = this.getByParentName(product, parentName);
        return attributes.filter(x => x.name == name).length > 0 ? attributes.filter(x => x.name == name)[0] : {
            value: "N/A"
        };
    }
    share(input) {
        this.copyMessage(btoa(this.pako.deflate(JSON.stringify(input), { to: 'string' })));
        alert("Share content copied to clipboard.");
    }
    fromShare(input) {
        return JSON.parse(this.pako.inflate(atob(input), { to: 'string' }));
    }
    copyMessage(val) {
        let selBox = document.createElement('textarea');
        selBox.style.position = 'fixed';
        selBox.style.left = '0';
        selBox.style.top = '0';
        selBox.style.opacity = '0';
        selBox.value = val;
        document.body.appendChild(selBox);
        selBox.focus();
        selBox.select();
        document.execCommand('copy');
        document.body.removeChild(selBox);
    }
};
UtilitiesService = __decorate([
    core_1.Injectable()
], UtilitiesService);
exports.UtilitiesService = UtilitiesService;
//# sourceMappingURL=utilities-service.js.map