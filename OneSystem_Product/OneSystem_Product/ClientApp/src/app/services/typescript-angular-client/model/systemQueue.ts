/**
 * My API
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */
import { SystemQueueUser } from './systemQueueUser';

export interface SystemQueue { 
    id?: number;
    entryDate?: Date;
    startDate?: Date;
    duration?: number;
    status?: string;
    response?: string;
    source?: string;
    action?: string;
    metadata?: string;
    hash?: string;
    actionShort?: string;
    userId?: number;
    pushReference?: number;
    pushTo?: string;
    pushDate?: Date;
    user?: SystemQueueUser;
}