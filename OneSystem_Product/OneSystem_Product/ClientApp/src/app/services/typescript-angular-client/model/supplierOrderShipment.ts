/**
 * My API
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */
import { SupplierOrder } from './supplierOrder';
import { SupplierOrderAddress } from './supplierOrderAddress';
import { SupplierOrderShipmentProduct } from './supplierOrderShipmentProduct';

export interface SupplierOrderShipment { 
    id?: number;
    supplierOrderId?: number;
    supplierShipId?: number;
    trackingNumber?: string;
    carrier?: string;
    shipMethod?: string;
    shipAccount?: string;
    fromAddressId?: number;
    toAddressId?: number;
    products?: Array<SupplierOrderShipmentProduct>;
    order?: SupplierOrder;
    from?: SupplierOrderAddress;
    to?: SupplierOrderAddress;
}