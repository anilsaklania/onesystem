/**
 * My API
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */
import { ProductSku } from './productSku';
import { ProductSkuPrefixSuffixAttribute } from './productSkuPrefixSuffixAttribute';

export interface ProductSkuPrefixSuffix { 
    id?: number;
    name?: string;
    value?: string;
    description?: string;
    isSuffix?: boolean;
    isActive?: boolean;
    productSkuPrefix?: Array<ProductSku>;
    productSkuSuffix?: Array<ProductSku>;
    attributes?: Array<ProductSkuPrefixSuffixAttribute>;
    lastModifiedDate?: Date;
    entryDate?: Date;
    lastUserId?: number;
    lastUsername?: string;
    userId?: number;
    username?: string;
}