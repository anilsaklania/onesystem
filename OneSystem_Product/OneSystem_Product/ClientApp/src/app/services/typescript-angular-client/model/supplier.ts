/**
 * My API
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */
import { SupplierAttribute } from './supplierAttribute';
import { SupplierOrder } from './supplierOrder';
import { SupplierProduct } from './supplierProduct';

export interface Supplier { 
    id?: number;
    name?: string;
    code?: string;
    isTracked?: boolean;
    isShipmentTracked?: boolean;
    isInventoryTracked?: boolean;
    isProductTracked?: boolean;
    isMediaTracked?: boolean;
    lastProductUpdate?: Date;
    products?: Array<SupplierProduct>;
    orders?: Array<SupplierOrder>;
    attributes?: Array<SupplierAttribute>;
}