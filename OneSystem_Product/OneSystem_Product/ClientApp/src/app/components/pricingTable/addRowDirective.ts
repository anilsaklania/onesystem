import { Directive, Input, HostListener } from "@angular/core";
import { Table } from "primeng-lts/table";

@Directive({
  selector: "[pAddRow]",
})
export class AddRowDirective {
  @Input() table: Table;
  @Input() newRow: any;

  @HostListener("click", ["$event"])
  onClick(event: Event) {
    debugger
    // Insert a new row
    this.table.value.push(this.newRow);

    //set the new row in edit mode
    this.table.initRowEdit(this.newRow);

    event.preventDefault();
  }
}
