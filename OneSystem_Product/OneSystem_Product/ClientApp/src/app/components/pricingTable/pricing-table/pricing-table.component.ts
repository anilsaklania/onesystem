import { ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, forwardRef, Input, OnInit, ViewChild } from "@angular/core";
import { TableModule } from "primeng-lts/table";
import { ConfirmationService } from "primeng-lts/api";
import { MessageService } from "primeng-lts/api";
import { NG_VALUE_ACCESSOR, NG_VALIDATORS, ControlValueAccessor, FormGroup, Validators, FormBuilder, FormControl, FormArray, ValidationErrors, ValidatorFn } from "@angular/forms";
import { Subscription } from "rxjs";
import { ColMandatory } from "../../productField/models/product-attr-input.models";

interface PriceData {
  id: number;
  min: number;
  max: number;
  salePrice?: number;
  salePriceExpire?: string;
  costPrice?: number;
  costPriceExpire?: string;
  shippingCost?: number;
  shippingCostExpire?: string;
  isNew?: boolean;
}

@Component({
  selector: "app-pricing-table",
  templateUrl: "./pricing-table.component.html",
  styleUrls: ["./pricing-table.component.css"],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => PricingTableComponent),
      multi: true,
    },
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => PricingTableComponent),
      multi: true,
    },
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PricingTableComponent implements OnInit, ControlValueAccessor {

  form: FormGroup;
  subscriptions: Subscription[] = [];
  clonedPrice: { [s: string]: any } = {};
  newRowData: PriceData;

  _columns: any[];
  @Input() set columns(cols: any[]) {
    if (cols) {
      for (var i = 0; i < cols.length; i++) {
        if (cols[i].expiration) {
          cols.splice(i + 1, 0, {
            field: cols[i].field + 'Expire',
            name: cols[i].name + ' Expiration Date',
            type: 'date'
          });
          i++;
        }
      }
    }
    this._columns = cols || [];
  }
  @Input() interval: number;

  get columns(): any[] {
    return this._columns;
  }



  @ViewChild('dt') dt: any;

  constructor(
    private messageService: MessageService,
    private confirmationService: ConfirmationService,
    private formBuilder: FormBuilder,
    private changeDetectorRef: ChangeDetectorRef
  ) {
    // create the inner form
    this.form = this.formBuilder.group({
      data: this.formBuilder.array([])
    });

    this.subscriptions.push(
      // any time the inner form changes update the parent of any change
      this.form.valueChanges.subscribe(value => {
        value = JSON.parse(JSON.stringify(value));
        value.data.forEach(element => { delete element.isNew });
        this.onChange(value);
        this.onTouched();
      })
    );
  }

  get control(): FormArray {
    return this.form.get('data') as FormArray;
  }

  ngOnDestroy() {
    this.subscriptions.forEach(s => s.unsubscribe());
  }

  onChange: any = () => { };
  onTouched: any = () => { };

  registerOnChange(fn) {
    this.onChange = fn;
  }

  writeValue(value) {
    if (value) {
      value.data.forEach(element => {
        this.addControlRow();
      });
      this.form.patchValue(value);
    }

    if (value === null) {
      let data = this.addPrice();
      this.addControlRow();
      this.form.patchValue({ data: [data] });
    }
  }

  registerOnTouched(fn) {
    this.onTouched = fn;
  }

  validate(_: FormControl) {
    return this.form.valid ? null : { child: { valid: false } };
  }

  ngOnInit() {

  }

  trackByFn(index: any, item: any) {
    return index;
  }

  editPrice(price) {
    this.clonedPrice[price.id] = { ...price };
  }

  savePrice(price, dt, tr, index) {
    let priceControl = this.control.controls[index] as FormGroup;
    this.markAllAsTouched(priceControl);
    Object.keys(priceControl.controls).forEach(key => {
      priceControl.get(key).updateValueAndValidity();
    });
    if (this.control.controls[index].invalid) {
      return;
    }
    delete this.clonedPrice[price.id];
    dt.saveRowEdit(price, tr);
  }

  cancelPrice(price, index: number) {
    if (this.clonedPrice[price.id] && !price.isNew) {
      this.control.controls[index].setValue(this.clonedPrice[price.id]);
      delete this.clonedPrice[price.id];
    }
    // delete row if it's new
    else if (price.isNew && this.control.controls.length > 1) {
      this.control.removeAt(index);
    }
  }

  deletePrice(index: number) {
    this.confirmationService.confirm({
      message: "Are you sure you want to remove this record?",
      header: "Confirmation Dialog",
      icon: "pi pi-exclamation-triangle",
      accept: () => {
        this.control.removeAt(index);
        this.messageService.add({
          severity: "success",
          summary: "Success",
          detail: "Pricing details removed succesfully.",
        });
      },
    });
  }

  addPrice() {
    let lastPrice, range;
    let data = this.control.value;
    if (this.control.controls.length) {
      lastPrice = data[data.length - 1];
      range = this.interval ? this.interval : lastPrice.max - lastPrice.min + 1;
    }
    else {
      lastPrice = { max: 0 };
      range = this.interval ? this.interval : 100;
    }
    this.newRowData = { ...this.newRowData, id: data.length + 1, min: Number(lastPrice.max) + 1, max: Number(lastPrice.max) + Number(range), isNew: true };
    return this.newRowData;
  }

  addRow(index) {
    this.addPrice();
    // Insert a new row
    this.dt.value.push(this.newRowData);
    //set the new row in edit mode
    this.dt.initRowEdit(this.newRowData);
    this.addControlRow(this.newRowData, index + 1);
  }

  //adds a new row
  addControlRow(value?, index?) {
    let group = {
      id: new FormControl(value ? value.id : null),
      min: new FormControl(value ? value.min : null, [this.rangeValidation, this.overlappingValidation]),
      max: new FormControl(value ? value.max : null, [this.rangeValidation, this.overlappingValidation]),
      isNew: new FormControl(value ? value.isNew : false),
    };
    this.columns.forEach(col => {
      group[col.field] = new FormControl(value ? value[col.field] : null,
        [...(this.isRequired(col.field) ? [Validators.required] : []),
        ...(this.isSalePrice(col.field) ? [this.salePriceValidation] : [])]);
    });
    if (index) {
      this.control.insert(index, new FormGroup(group));
    }
    else {
      this.control.push(new FormGroup(group));
    }
  }

  // min should always be less than max
  rangeValidation: ValidatorFn = (fg: FormControl) => {
    let priceControl = fg.parent;
    if (!priceControl) return null;
    Object.keys(priceControl.controls).forEach(key => { });
    let min = priceControl.get('min').value;
    let max = priceControl.get('max').value;

    if (min > max) {
      return { range: true, message: 'Max must be greater than Min' };
    }
    return null;
  }

  //min-max should be exclusive
  overlappingValidation: ValidatorFn = (fg: FormControl) => {
    let priceControl = fg.parent;
    if (!priceControl) return null;

    let min = priceControl.get('min').value;
    let max = priceControl.get('max').value;
    let id = priceControl.get('id').value;
    let selected = { min, max, id };

    let priceArray = priceControl.parent;
    if (!priceArray) return null;
    const isOverlapping = priceArray.value.some(price => price.id != selected.id && (
      (price.min <= selected.min && price.max >= selected.min) ||
      (price.min <= selected.max && price.max >= selected.max) ||
      (price.min >= selected.min && price.max <= selected.max))
    );

    if (isOverlapping) {
      return { range: true, message: 'Min-Max overlapping' };
    }

    return null;
  }

  // salePrice should be less for large quantity
  salePriceValidation: ValidatorFn = (fg: FormControl) => {
    let priceControl = fg.parent;
    if (!priceControl) return null;

    let min = priceControl.get('min').value;
    let max = priceControl.get('max').value;
    let salePriceKey = '';

    Object.keys(priceControl.controls).forEach(key => {
      let childControl = priceControl.get(key);
      if (childControl !== fg) {
        return;
      }
      salePriceKey = key;
    });

    let salePrice = priceControl.get(salePriceKey).value;

    let priceArray = priceControl.parent;
    if (!priceArray) return null;

    const isSalePriceInvalid = priceArray.value.some(price => (min > price.max && salePrice > price[salePriceKey]) ||
      (max < price.min && salePrice < price[salePriceKey]));

    if (isSalePriceInvalid) {
      return { invalid: true, message: 'Sale Price must be less for more quantity' };
    }

    return null;
  }

  // to copy the current value to below columns
  quickFill(field, index) {
    this.confirmationService.confirm({
      message: "Are you sure you want to copy below this value?",
      header: "Confirmation Dialog",
      icon: "pi pi-exclamation-triangle",
      accept: () => {
        let currentValue = this.control.controls[index].get(field).value;
        for (var i = index; i < this.control.controls.length; i++) {
          this.control.controls[i].get(field).setValue(currentValue);
        }
      },
    });
  }

  startFill(field, index) {
    let currentValue = this.control.controls[index].get(field).value;
    this.newRowData[field] = currentValue;
  }

  // Cell Validation
  isRequired(key) {
    let column = this.columns.find(x => x.field == key);
    if (column != null) {
      if (column.colMandatory == ColMandatory.All)
        return true;
      else if (column.colMandatory == ColMandatory.AtleastOne) {
        return this.control.controls.length < 1 ? true : false;
      }
      else { // ColMandatory.Optional case
        return false;
      }
    }
  }

  // check if column is salePrice
  isSalePrice(key) {
    let column = this.columns.find(x => x.field == key);
    if (column != null) {
      return column.salePrice;
    }
  }

  // show error in title
  getErrors(index, field) {
    if (field) {
      const controlErrors: ValidationErrors = this.control.controls[index].get(field).errors;
      if (controlErrors) {
        console.log(controlErrors);
        if (controlErrors.required) {
          return 'This field is required';
        }
        return controlErrors.message || '';
      }
    }
    return '';
  }

  markAllAsTouched(formGroup: FormGroup): void {
    (Object as any).values(formGroup.controls).forEach((control: any) => {
      control.markAsTouched();
      control.markAsDirty();
      if (control.controls) {
        this.markAllAsTouched(control);
      }
    });
  }
}

