import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SHP735Component } from './shp735.component';

describe('SHP735Component', () => {
  let component: SHP735Component;
  let fixture: ComponentFixture<SHP735Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SHP735Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SHP735Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
