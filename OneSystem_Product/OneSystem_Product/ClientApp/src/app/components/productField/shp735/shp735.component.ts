import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup } from "@angular/forms";
import {
  AttributeInputData,
  shpData,
  shpPricingCols,
} from "../models/product-attr-input.models";

@Component({
  selector: "app-shp735",
  templateUrl: "./shp735.component.html",
  styleUrls: ["./shp735.component.css"],
})
export class SHP735Component implements OnInit {
  public productForm: FormGroup;
  constructor(private formBuilder: FormBuilder) {
    this.productForm = this.formBuilder.group({
      productSize: [new AttributeInputData()],
      productSize1: [new AttributeInputData()],
      productSize2: [new AttributeInputData()],
      productSize10: [new AttributeInputData()],
      productSize11: [new AttributeInputData()],
      productSize12: [new AttributeInputData()],
      productSize13: [new AttributeInputData()],
      pricingData: [],
    });
  }
  pricingCols: any[];

  ngOnInit(): void {
    this.pricingCols = shpPricingCols;
    //if edit - call api to get the data
    let edit = true;
    if (edit) {
      this.getApiData();
    }
  }

  // handle form submission
  handleSubmitForm() {
    if (this.productForm.valid) console.log(this.productForm.value);
    else alert("invalid data");
  }

  // get saved form data
  getApiData() {
    this.productForm.patchValue(shpData);
  }
}
