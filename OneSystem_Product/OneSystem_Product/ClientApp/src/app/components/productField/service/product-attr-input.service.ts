import { AttributeInputConfig, AttributeInputDataType, AttributeInputRuleType } from '../models/product-attr-input.models';
import { Injectable } from "@angular/core";

@Injectable()
export class AttributeInputService { //attr-input-service
 
  
  getControlConfig(id : number) : AttributeInputConfig
  {
    var objControlConfig = new AttributeInputConfig();
    objControlConfig.id = id;
    objControlConfig.datatype = AttributeInputDataType.Text;
    objControlConfig.rule = AttributeInputRuleType.Any;
    objControlConfig.label =  "attribute-" + id;
  //call api to get the details *****
    return objControlConfig;
  }
  
  }
  