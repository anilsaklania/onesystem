export const productFieldConstants = {
  regex:
  {
    as400_name: '^[A-Z0-9 :"\':()/-]{1,31}'
  },
  template: { 
  },
  pricingCols: [{
    name: "Regular Price",
    field: "regPrice",
    salePrice: false,
    colMandatory: 1,
    precision: 2,
    type: "currency",
  },
  {
    name: "Sale Price",
    field: "salePrice",
    salePrice: true,
    expiration: true,
    colMandatory: 3,
    precision: 2,
    type: "currency",
    },
    {
      name: "Setup Price",
      field: "setupPrice",
      salePrice: false,
      colMandatory: 3,
      precision: 2,
      type: "currency",
    }, 
  {
    name: "Cost",
    precision: 4,
    field: "cost",
    type: "currency",
    colMandatory: 2,
    toolTip: "Cost",
    quickFill: true,
  },
  {
    name: "Landed", field: "fobCost", type: "currency",
    colMandatory: 3, 
    quickFill: true,
    precision: 4,
    toolTip: "Supply Chain Cost"
  },
  ],
  pricingFirstRow: {
    id: 1,
    min: 1,
    max: 50,
    regPrice: 0,
    salePrice: 0,
    costPrice: 0,
    fobCost: 0,
    salePriceExpire: "01/01/2030",
  },
  childAdditionalAttributes: [ 
    {
      parentAttributeId: 240235, 
      name: 'Offered On Web',
      type: 'PWEBYN',
      value: 'N' 
    }
  ]

};
