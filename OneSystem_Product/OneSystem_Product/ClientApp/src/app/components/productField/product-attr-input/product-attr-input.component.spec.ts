import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductAttrInputComponent } from './product-attr-input.component';

describe('ProductAttrInputComponent', () => {
  let component: ProductAttrInputComponent;
  let fixture: ComponentFixture<ProductAttrInputComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProductAttrInputComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductAttrInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
