import { SelectItem } from "primeng-lts/api";
import {
  Component,
  Input,
  OnDestroy,
  ChangeDetectionStrategy,
  forwardRef,
  OnInit,
  EventEmitter,
  ChangeDetectorRef,
  Output,
  AfterViewInit,
  ViewChild,
} from "@angular/core";
import {
  ControlValueAccessor,
  NG_VALUE_ACCESSOR,
  FormBuilder,
  FormGroup,
  Validators,
  FormControl,
  NG_VALIDATORS,
  FormArray,
} from "@angular/forms";
import { Subscription } from "rxjs";
import { AttributeInputService } from "../service/product-attr-input.service";
import {
  AttributeInputData,
  AttributeInputDataRow,
  AttributeInputControlRow,
  AttributeInputConfig,
  AttributeInputDataType,
  AttributeInputRuleType,
} from "../models/product-attr-input.models";
import { AutoCompleteModule } from "primeng-lts/autocomplete";
import {
  ProductAttribute,
  ProductService,
  Rule,
} from "../../../services/typescript-angular-client";
import { OdataService } from "../../../services/odata.service";
import { OverlayPanel } from "primeng-lts/overlaypanel";

@Component({
  selector: "app-product-attr-input",
  templateUrl: "./product-attr-input.component.html",
  styleUrls: ["./product-attr-input.component.css"],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => ProductAttrInputComponent),
      multi: true,
    },
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => ProductAttrInputComponent),
      multi: true,
    },
    ProductService,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProductAttrInputComponent implements OnInit, ControlValueAccessor {
  constructor(
    private productService: ProductService,
    private formBuilder: FormBuilder,
    private dataService: AttributeInputService,
    private changeDetectorRef: ChangeDetectorRef, private odata: OdataService
  ) {
    // inner form
    this.productFieldForm = this.formBuilder.group({
      attributeId: this.id,
      nameValueCol: this.formBuilder.array([]),
    });

    this.subscriptions.push(
      // any time the inner form changes update the parent of any change
      this.productFieldForm.valueChanges.subscribe((value) => {
        this.onChange(value);
        this.onTouched();
      })
    );
  }

  ngOnInit() {
    if (this.resolution) {
      this.resolve = JSON.parse(this.resolution);
      this.resolve.forEach(x => {
        this.odata.query("product/statictypereference", `$filter=Source eq '${x.source}'  and Key eq '${x.key}'`).subscribe(r => {
          x.resolved = r;
        });
      });
    }
    this.setParentAttribute();
  }
  
  resolutionContains(area: string, idx): boolean {
    return this.resolve.findIndex(x => x.area == area && x.key == this.controlRowCol.controls[idx].get('name').value) > -1; 
  }
  renderResolveOverlay(area: string, evt, idx) {
    this.resolutionControl = {
      area: area,
      idx: idx
    }; 
    this.resolutionOptions = []; 
    let itm = this.resolve.filter(x => x.area == area && x.key == this.controlRowCol.controls[idx].get('name').value)[0];
    this.resolutionOptions = itm.resolved;
    this.op.show(evt);
  }
  resolveControl(evt) { 
    this.controlRowCol.controls[this.resolutionControl.idx].get(this.resolutionControl.area).setValue(evt.value[0]['type']);
    this.op.hide();
  }
  // Input Attributes - Passed from Parent Form component
  @Input() id: number;
  @Input() label: string;
  @Input() tiedOptions: any[] = null;
  @Input() allowNewRow = false;
  @Input() allowMultipleValues = false;
  @Input() regex: string;
  @Input() standAlone = false;
  @Input() ignoreParent = false;
  @Input() nameOnly = false;
  @Input() nameRequired = true;
  @Input() valueRequired = false;
  @Input() tiedOptionRequired = false;
  @Input() disableName: boolean = false;
  @Input() disableValue: boolean = false;
  @Input() disableTied: boolean = false;
  @Input() nameLabel: string = "Name";
  @Input() valueLabel: string = "Value";
  @Input() tiedLabel: string = "Tied Option";
  @Input() cssClass: string;
  @Input() expanded: boolean = false;
  @Input() valueCapitalize: boolean = true;
  @Input() orientation: string = 'V';
  @Input() ignoreSpecificValue: boolean = false;
  @Input() resolution: string;
  dataType = String(AttributeInputDataType.Text);
  rule = "";

  tempName: string;
  resolve: any[];
  resolutionOptions: any[];
  resolutionControl: any; 
  filterRules: any[] = [];
  parentAttribute: ProductAttribute[] = [];
  childAttributes: ProductAttribute[] = [];
  childAttributesTied: ProductAttribute[][] =[];

  // control (dropdown/autocomplete) suggestions
  nameResults: string[] = [];
  valueResults: any[] = [];
  valueResultsTied: any[][] = [];
  tiedOptionsResults: any[] = [];
  specificResults: any[] = [];
  originalSpecificResults: any[] = [];
  valueMemory: any[] = []; 
  // for lazy loading
  skip: number = 0;
  take: number = 10;

  public selectedSpecificOptions: Array<any> = new Array<any>();
  public selectedItemsLabel = "Choose";
  public hasSpecificValue: boolean = false;

  subscriptions: Subscription[] = [];

  //Inner Form
  productFieldForm: FormGroup;
  // data accessor
  data: AttributeInputData;

  //Form ControlValueAccessor interface
  onChange: any = () => { };
  onTouched: any = () => { };
  @ViewChild('op') op: OverlayPanel;
  registerOnChange(fn: any) {
    this.onChange = fn;
    if (this.data == null) {
      this.onChange(this.productFieldForm.value);
    }
  }
  registerOnTouched(fn: any) {
    this.onTouched = fn;
  }

  // called when parent form changes -> update inner form
  writeValue(inputData: AttributeInputData): void {
    if (inputData) {
      this.controlRowCol.removeAt(0);
      inputData.nameValueCol.forEach((item, index) => {
        this.addControlRow(index);
      });
      inputData.attributeId = this.id;
      this.productFieldForm.patchValue(inputData);
      this.data = inputData;
    }
    if (inputData === null) {
      this.productFieldForm.reset();
    }
  }

  // communicate the inner form validation to the parent form
  validate(_: FormControl) {
    return this.productFieldForm.valid ? null : { childForm: { valid: false } };
  }

  get controlRowCol(): FormArray {
    return this.productFieldForm.get("nameValueCol") as FormArray;
  }

  setParentAttribute() {
    // Set parent filter attribute
    this.filterRules.push({
      field: "Id",
      filterMode: "Equal",
      searchQuery: this.id,
    });
    this.productService
      .apiProductGetProductAttributesSearchPost({
        filter: this.buildRule(),
      })
      .subscribe((x) => {
        this.parentAttribute = x;
        if (this.parentAttribute && this.parentAttribute.length) {
          let name = this.controlRowCol.controls[0].get("name").value || this.parentAttribute[0].name;
          let value = this.parentAttribute[0].value;
          let type =
            this.parentAttribute[0].audit && this.parentAttribute[0].audit.type;
          if (
            type &&
            type.toLowerCase() == AttributeInputRuleType.Specefic.toLowerCase()
          ) {
            this.rule = AttributeInputRuleType.Specefic;
            this.dataType = AttributeInputDataType.Text;
            if (!this.standAlone) {
              this.allowNewRow = false;
              this.take = 100;
              // get childAttributes on load - for specific
              this.setChildAttributes("", "").then((x) => {
                this.originalSpecificResults = [].concat(this.childAttributes);
                this.specificResults = Object.values(
                  this.groupByName(this.originalSpecificResults, "name")
                );
                // called for setting the default data in specific
                this.setDefaultSpecificData();
              });
            }
          } else if (
            type &&
            type.toLowerCase() == AttributeInputRuleType.Limit.toLowerCase()
          ) {
            this.rule = AttributeInputRuleType.Limit;
            this.dataType = AttributeInputDataType.Text;
          } else {
            this.rule = AttributeInputRuleType.Any;
            this.dataType = AttributeInputDataType.Text;
          }

          if (!this.label && !this.standAlone && !this.nameOnly) {
            this.label = value ? value : name;
          }

          // handle StandAlone Case -Value Only
          if (this.standAlone) {
            this.handleStandAlone(name, value);
          }

          // handle Nameonly case
          if (this.nameOnly) {
            this.nameLabel = this.label || value||name; 
            this.label = "";
          }

          if (this.tiedOptions && this.tiedOptions.length) { 
            if (this.orientation == 'V')
              this.autoAddTieds(this.tiedOptions, '', 0);
            else
              this.expandTieds(this.tiedOptions,  0);
          }

          this.changeDetectorRef.markForCheck();
        }
      });
  }

  setChildAttributes(
    searchField: string,
    valStartsWith: string,
    index?: number, idx? : number
  ) {
    return new Promise<void>((resolve, reject) => {
      this.filterRules = [];
      if (!this.ignoreParent) {
        this.filterRules.push({
          field: "ParentAttributeId",
          filterMode: "Equal",
          searchQuery: this.id,
        });
      }

      if (searchField) {
        this.filterRules.push({
          field: searchField,
          filterMode: "StartsWith",
          searchQuery: valStartsWith,
        });
      }

      //in case of value
      if (searchField == "Value") {
        let nameText = this.standAlone ? this.tempName : this.controlRowCol.controls[index].get("name").value;
        if (nameText) {
          this.filterRules.push({
            field: "Name",
            filterMode: "Equal",
            searchQuery: nameText,
          });
        }
      }
      if (idx != undefined) {
          this.filterRules.push({
            field: "Type",
            filterMode: "Equal",
            searchQuery: this.tiedOptions[idx],
          }); 
      }
      // in case of tied option, send name and value in filters
      if (searchField == "Type") {
        let nameText = this.controlRowCol.controls[index].get("name").value;
        let valueText = this.controlRowCol.controls[index].get("value").value;
        if (nameText) {
          this.filterRules.push({
            field: "Name",
            filterMode: "Equal",
            searchQuery: nameText,
          });
        }
        if (valueText && !this.allowMultipleValues) {
          this.filterRules.push({
            field: "Value",
            filterMode: "Equal",
            searchQuery: valueText,
          });
        }
      }

      if (this.rule == AttributeInputRuleType.Specefic || (this.rule == AttributeInputRuleType.Limit && searchField == "Name")) {
        this.filterRules.push({
          field: "AuditId",
          filterMode: "HasValue"
        });
      }

      let filterObj = {
        filter: this.buildRule(),
        ...(searchField && searchField != "Value" && { distinctBy: searchField, distinct: true }),
        start: this.skip,
        size: this.take,
      };

      this.productService
        .apiProductGetProductAttributesSearchPost(filterObj)
        .subscribe((x) => {
          if (idx != undefined && this.orientation == 'H')
            this.childAttributesTied[idx] = x; 
          else
            this.childAttributes = x;
          resolve();
        });
    });
  }

  //handle standAlone case
  handleStandAlone(name, value) {
    while (this.controlRowCol.length != 1) {
      this.controlRowCol.removeAt(1);
    }
    if (this.rule == AttributeInputRuleType.Specefic) {
      const obj = [{ name, value }];
      this.specificResults = Object.values(this.groupByName(obj, "name"));
      this.controlRowCol.controls[0].get("name").setValue(name);
      this.controlRowCol.controls[0].get("value").setValue([value]);
      this.selectedItemsLabel = `${name}-${value}`;
    } else if (this.rule == AttributeInputRuleType.Limit) {
      let nameValue = this.controlRowCol.controls[0].value.name;
      this.valueLabel = this.label ? this.label : (nameValue ? nameValue : name);
      this.nameResults.push(name);
      this.tempName = nameValue;
      this.controlRowCol.controls[0].get("name").setValue(name);
    }
    this.label = "";
    this.allowMultipleValues = false;
    this.allowNewRow = false;
  }

  // handle click on delete button
  deleteControlRow(i) {
    this.controlRowCol.removeAt(i);
  }

  // handle click on add button
  addControlRow(id: number, index?): void {
    const objControlRow = new AttributeInputControlRow();
    objControlRow.childAttributeId = id;
    objControlRow.name = new FormControl("", [
      ...(this.nameRequired ? [Validators.required] : []),
      ...(this.regex ? [Validators.pattern(this.regex)] : []),
    ]);

    objControlRow.value = new FormControl("", [
      ...(this.valueRequired ? [Validators.required] : []),
      ...(this.regex ? [Validators.pattern(this.regex)] : []),
    ]);

    objControlRow.tiedAttributeValue = new FormControl("", [
      ...(this.tiedOptionRequired ? [Validators.required] : []),
    ]);

    //-- for allowNewRow when orientation H add copy of tied 
    if (this.allowNewRow && this.orientation == 'H' ) {
      for (let i = 0; i < this.tiedOptions.length; i++)
        objControlRow[this.tiedOptions[i]] = new FormControl("");
    }
    var name = new FormControl("");
    var value = new FormControl("");
    var tied = new FormControl("");
    objControlRow.disableProp = new FormGroup({ name, value, tied });
    const group = this.formBuilder.group(objControlRow);
    if (index) {
      this.controlRowCol.insert(index, group);
    }
    else {
      this.controlRowCol.push(group);
    }
  }

  // handle name search
  filterName(event, i) {
    this.skip = 0;
    const nameLike = event.query.toLowerCase();
    this.setChildAttributes("Name", nameLike).then(() => {
      this.nameResults = this.childAttributes.map((x) => x.name);
      this.changeDetectorRef.markForCheck();
      this.addScrollEvent("name", i);
    });
  }

  // handle value search
  filterValue(event, i, idx?) {
    this.skip = 0;
    const valueLike = event.query.toLowerCase(); 
    this.setChildAttributes("Value", valueLike, i, idx).then(() => {
      if (idx === undefined || this.orientation == 'V') {
        this.valueResults = this.childAttributes.map((x) => x.value);
        if (this.valueResults.length == 0)
          this.valueMemory.splice(i, 0, valueLike);
      } else 
        this.valueResultsTied[idx] = this.childAttributesTied[idx].map((x) => x.value);
      if (((idx === undefined || this.orientation == 'V') && this.valueResults.length > 0) || ((idx != undefined || this.orientation == 'H')&&this.valueResultsTied[idx].length>0))
      this.changeDetectorRef.markForCheck(); 
      this.addScrollEvent("value", i, idx, valueLike);
    });
  }

  // handle tied search
  filterTied(event, i) {
    this.skip = 0;
    const tiedLike = event.query.toLowerCase();
    if (this.tiedOptions.length) {
      const filter = this.tiedOptions.filter((option) =>
        option.includes(event.query)
      );
      this.tiedOptionsResults = filter;
    } else {
      this.setChildAttributes("Type", tiedLike, i).then(() => {
        this.tiedOptionsResults = this.childAttributes.map((x) => x.type);
        this.changeDetectorRef.markForCheck();
        this.addScrollEvent("type", i);
      });
    }
  }

  // auto set tied attribute value based on name
  onNameSelect(e, index) {
    let text = e.target.value.toUpperCase();

    if (text && text != e.target.defaultValue) {
      this.filterRules = [];
      this.filterRules.push({
        field: "ParentAttributeId",
        filterMode: "Equal",
        searchQuery: this.id,
      });
      this.filterRules.push({
        field: "Name",
        filterMode: "Equal",
        searchQuery: text,
      });
      let filterObj = {
        filter: this.buildRule(),
        start: this.skip,
        size: this.take,
      };

      this.productService
        .apiProductGetProductAttributesSearchPost(filterObj)
        .subscribe((x) => {
          this.childAttributes = x;
          let tieds = this.childAttributes
            .map((x) => x.type)
            .filter((x) => x)
            .filter((x, i, a) => a.indexOf(x) == i);

          this.controlRowCol.controls = this.controlRowCol.controls.filter(x => x.get('name').value != e.target.defaultValue);
          if (this.orientation =='V')
          this.autoAddTieds(tieds, text, index);
          e.target.defaultValue = text;
          this.changeDetectorRef.markForCheck();
        });
    }
  }

  autoAddTieds(tieds, name, index) {
    for (let i = index; i < index + tieds.length; i++) {
      if (i != index) {
        this.addControlRow(i + 1, i);
        if (name) {
          this.controlRowCol.controls[i].get("name").disable({ onlySelf: true });
        }
      }
      this.controlRowCol.controls[i].get("name").setValue(name);

      this.controlRowCol.controls[i].get("tiedAttributeValue").setValue(tieds[i - index]);
    }
    if (!tieds.length) {
      this.controlRowCol.controls[index].get("tiedAttributeValue").setValue('');
    }
  }
  expandTieds(tieds: any[], index) {
    //this.controlRowCol.controls[index].get("tiedAttributeValue").setValue(tieds.map(x => null));
    this.childAttributesTied = tieds.map(x => []);
   // this.controlRowCol.controls[index](new AttributeInputData())
  }
  setName() {
    if (this.tiedOptions && this.tiedOptions.length && this.orientation == 'V') {
      let name = this.controlRowCol.controls[0].get("name").value;
      this.tiedOptions.forEach((tied, i) => {
        this.controlRowCol.controls[i].get('name').setValue(name);
      });
    }
  }

  ifValueAlreadyExists(value, i) {
    return this.controlRowCol.controls[i].value.value.some(
      (x) => x.toLowerCase() == value.toLowerCase()
    );
  }

  //handle allowMultipleValues attribute - custom
  onValueKeyUp(event, index) {
    this.controlRowCol.controls[index].get('name').setErrors({ invalid: true });
    let text = event.target.value;
    if (event.keyCode == 13 && text) {
      if (this.controlRowCol.controls[index].get("value").value === "")
        this.controlRowCol.controls[index].get("value").setValue([]);
      if (!this.ifValueAlreadyExists(text, index)) {
        let value = this.controlRowCol.controls[index].get("value").value;
        value.push(this.valueCapitalize ? text.toUpperCase() : text);
        this.controlRowCol.controls[index].get("value").setValue(value);
      }
      event.target.value = "";
    }
  }

  // build filterRules
  buildRule(): Rule[] {
    let fil = [];
    this.filterRules.forEach((x) => {
      fil.push({
        memberName: `${x.field}`,
        operator: `${x.filterMode}`,
        ...(x.hasOwnProperty('searchQuery') && { targetValue: `${x.searchQuery}` }),
      });
    });
    return fil;
  }

  // groups the data by key (used to create specific data)
  groupByName = (xs, key) => {
    return xs.reduce((rv, x) => {
      if (!rv[x[key]]) {
        rv[x[key]] = {
          name: x.name,
          value: { name: x.name, childOptions: [], selectedChildOptions: [] },
        };
      }
      if (x.value && !this.ignoreSpecificValue) {
        rv[x[key]]["value"]["childOptions"].push({
          name: x.value,
          value: { name: x.value, childOptions: [] },
        });
        this.hasSpecificValue = true;
      }
      return rv;
    }, {});
  };

  /* specific custom control functions */
  public onChangeMultiSelect($event) {
    if ($event.itemValue) {
      // this is when a single multi select option is clicked
      this.handleMultiSelectOptionClicked($event);
      let selected = this.specificResults.filter(
        (x) => x.value.selectedChildOptions.length > 0
      );
      if (!selected.length) {
        selected = $event.value;
      }
      let selectedLength = selected.length;
      let controlRowLength = this.data.nameValueCol.length;
      if (selectedLength == 1) {
        let value = $event.value[0].selectedChildOptions.map((x) => x.name);
        this.controlRowCol.controls[0]
          .get("name")
          .setValue(selectedLength ? $event.value[0].name : "");
        this.controlRowCol.controls[0].get("value").setValue(value);
      }
      if (controlRowLength < selectedLength) {
        this.data.nameValueCol.push(new AttributeInputDataRow(selectedLength));
      } else {
        this.data.nameValueCol.splice(
          selectedLength == 0 ? 1 : selectedLength,
          controlRowLength - selectedLength
        );
      }
      selected.forEach((element, index) => {
        let value = element.value && element.value.selectedChildOptions.map((x) => x.name);
        this.data.nameValueCol[index].name = selectedLength ? element.name : "";
        this.data.nameValueCol[index].value = value;
      });
      this.onChange(this.data);
      if (!selectedLength) {
        this.controlRowCol.controls[0].get("name").setValue("");
        this.controlRowCol.controls[0].get("value").setValue([]);
      }
    }
    this.controlRowCol.controls[0].get("name").markAsTouched();
  }

  private handleMultiSelectOptionClicked(event) {
    let elementChecked = false;
    event.value.forEach((element) => {
      if (element.name === event.itemValue.name) {
        elementChecked = true;
      }
    });
    if (elementChecked) {
      // this is when element is checked
      this.specificResults.forEach((option) => {
        if (option.value.name === event.itemValue.name) {
          option.value.selectedChildOptions = [];
          event.itemValue.childOptions.forEach((element) => {
            option.value.selectedChildOptions.push(element.value);
          });
        }
      });
    } else {
      // this is when element is unchecked
      this.specificResults.forEach((option) => {
        if (option.value.name === event.itemValue.name) {
          option.value.selectedChildOptions = [];
        }
      });
    }
  }

  // handle change of value checkbox
  public onChangeChildOptionCheckbox(option) {
    // $event is true or false depending on whether its checked or unchecked.
    if (
      option.value.childOptions.length !==
      option.value.selectedChildOptions.length
    ) {
      this.selectedSpecificOptions = this.selectedSpecificOptions.filter(
        (selectedOption) => selectedOption["name"] !== option.value.name
      );
    } else {
      this.selectedSpecificOptions = [...this.selectedSpecificOptions];
      this.selectedSpecificOptions.push(option.value);
    }
    let index = -1;
    this.data.nameValueCol.forEach((element, i) => {
      if (element.name == option.name) {
        index = i;
      }
    });
    let controlRowLength = this.data.nameValueCol.length;
    let value = option.value.selectedChildOptions.map((x) => x.name);
    if (index == -1) {
      if (this.controlRowCol.controls[0].get("name").value != "") {
        this.data.nameValueCol.push(
          new AttributeInputDataRow(controlRowLength + 1)
        );
        index = controlRowLength;
      } else {
        index = 0;
      }
    }
    if (controlRowLength > 1 && !value.length) {
      this.data.nameValueCol.splice(index, 1);
    } else {
      if (index == 0) {
        this.controlRowCol.controls[0]
          .get("name")
          .setValue(value.length ? option.name : "");
        this.controlRowCol.controls[0].get("value").setValue(value);
      }
      this.data.nameValueCol[index].name = value.length ? option.name : "";
      this.data.nameValueCol[index].value = value;
    }
    this.controlRowCol.controls[0].get("name").markAsTouched();
    this.onChange(this.data);
  }

  public onClickChildOptionChecbox(event) {
    event.stopPropagation();
  }

  public onClickChildOptionContainer(event) {
    event.stopPropagation();
  }

  // creates label that is displayed on the control based on selection
  public getSelectedItemsLabel() {
    let selectedItemLabels = [];
    this.specificResults.forEach((option) => {
      // if all values are selected then name is shown
      if (
        option.value.childOptions.length ===
        option.value.selectedChildOptions.length &&
        option.value.childOptions.length != 0
      ) {
        selectedItemLabels.push(option.name);
      } else {
        // if some values are selected then name-value is shown
        option.value.selectedChildOptions.forEach((childOption) => {
          selectedItemLabels.push(`${option.value.name} - ${childOption.name}`);
        });
      }
    });
    if (!selectedItemLabels.length) {
      let items = this.selectedSpecificOptions.map((x) => x["name"]);
      selectedItemLabels = [...items];
    }
    return selectedItemLabels.length > 0
      ? selectedItemLabels.join(",")
      : this.selectedItemsLabel;
  }

  // sets data set by parent in specific control
  private setDefaultSpecificData() {
    this.data.nameValueCol.forEach((element) => {
      this.specificResults.forEach((x, i) => {
        if (x.name == element.name) {
          // for name and value
          let options = x.value.childOptions
            .filter((a) => element.value.indexOf(a.name) != -1)
            .map((x) => x.value);
          x.value.selectedChildOptions = [...options];
          // for name only
          if (
            x.value.childOptions.length == x.value.selectedChildOptions.length
          ) {
            this.selectedSpecificOptions.push(x.value);
          }
        }
      });
    });
    this.changeDetectorRef.markForCheck();
  }
  /*end of specific custom control functions */

  ngOnDestroy() {
    this.subscriptions.forEach((s) => s.unsubscribe());
  }

  // attach scroll event for any and limit case
  addScrollEvent(control, i, idx?, forceVal?) {
    setTimeout(() => {
      let panel = <HTMLElement>(
        document.body.querySelector(".p-autocomplete-panel")
      );
      if (panel != null && panel.getAttribute("listener") !== "true") {
        panel.addEventListener("scroll", () => {
          if (panel.scrollHeight - panel.scrollTop == 198) {
            console.log("Panel Scroll reached bottom");
            if (control == "name") {
              this.skip = this.nameResults.length;
              let value = this.controlRowCol.controls[i].get("name").value;
              this.setChildAttributes("Name", value || "").then((x) => {
                this.nameResults = this.nameResults.concat(
                  this.childAttributes.map((x) => x.name)
                );
                this.changeDetectorRef.markForCheck();
              });
            } else if (control == "value") {
              this.skip = idx != undefined ? this.valueResultsTied[idx].length : this.valueResults.length;
              let value = idx != undefined ? this.controlRowCol.controls[i].get(`${this.tiedOptions[idx]}`).value : forceVal||this.controlRowCol.controls[i].get("value").value;
            
              this.setChildAttributes("Value", value || "", i, idx).then((x) => {
                if (idx === undefined || this.orientation == 'V')
                this.valueResults = this.valueResults.concat(
                  this.childAttributes.map((x) => x.value)
                  );
                else
                  this.valueResultsTied[idx] = this.valueResultsTied[idx].concat(
                    this.childAttributesTied[idx].map((x) => x.value)
                  );
                this.changeDetectorRef.markForCheck();
              });
            } else if (control == "type") {
              this.skip = this.tiedOptionsResults.length;
              let value = this.controlRowCol.controls[i].get(
                "tiedAttributeValue"
              ).value;
              this.setChildAttributes("Type", value || "", i).then((x) => {
                this.tiedOptionsResults = this.tiedOptionsResults.concat(
                  this.childAttributes.map((x) => x.type)
                );
                this.changeDetectorRef.markForCheck();
              });
            }
          }
        });
        panel.setAttribute("listener", "true");
      } else {
        console.log("Unable to catch the panel");
      }
    }, 1000);
  } 
  // attach scroll event for specific case
  onPanelShow() {
    setTimeout(() => {
      let panel = <HTMLElement>(
        document.body.querySelector(".p-multiselect-items-wrapper")
      );
      if (panel != null && panel.getAttribute("listener") !== "true") {
        panel.addEventListener("scroll", () => {
          if (panel.scrollHeight - panel.scrollTop == 200) {
            console.log("Panel Scroll reached bottom");
            this.skip = this.originalSpecificResults.length;
            this.setChildAttributes("", "").then((x) => {
              this.originalSpecificResults = this.originalSpecificResults.concat(
                this.childAttributes
              );
              let data = Object.values(
                this.groupByName(this.originalSpecificResults, "name")
              );
              this.specificResults = data;
              this.changeDetectorRef.markForCheck();
            });
          }
        });
        panel.setAttribute("listener", "true");
      } else {
        console.log("Unable to catch the panel");
      }
    }, 1000);
  }

  onForceAdd(idx, i) {
    let valControl = this.controlRowCol.controls[i].get('value');
    let cur = <string[]>valControl.value;
    cur.push(this.valueMemory[i]);
    valControl.setValue(cur);
  }
}
