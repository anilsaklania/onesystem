import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup } from "@angular/forms";
import {
  AttributeInputData,
  AttributeInputDataRow,
  pricingCols,
} from "../models/product-attr-input.models";
import { data } from "../models/product-attr-input.models";

@Component({
  selector: "app-product-form",
  templateUrl: "./product-form.component.html",
  styleUrls: ["./product-form.component.css"],
})
export class ProductFormComponent implements OnInit {
  public productForm: FormGroup;
  constructor(private formBuilder: FormBuilder) {
    this.productForm = this.formBuilder.group({
      as400Name: [
        new AttributeInputData(1, [
          new AttributeInputDataRow(1, "Name", [], "POITDS", {
            name: true,
          }),
        ])
      ], as400VendorCode: [
        new AttributeInputData(1, [
          new AttributeInputDataRow(1, "Vendor Code", [], "POVNDI", {
            name: true,
          }),
        ])
      ], as400ManufacturingType: [
        new AttributeInputData(1, [
          new AttributeInputDataRow(1, "Manufacturing Type", [], "POVNDI", {
            name: true,
          }),
        ])
      ],
      as400VendorSku: [new AttributeInputData(1, [
        new AttributeInputDataRow(1, "Vendor Code", [], "POVNDI", {
          name: true
        })
      ])
      ],
      as400VendorSku1: [new AttributeInputData(1, [
        new AttributeInputDataRow(1, "Vendor Code", [], "POVNDI", {
          name: true
        })
      ])
      ],
      as400VendorSku2: [new AttributeInputData(1, [
        new AttributeInputDataRow(1, "", [], "POVNDI", {
          name: true
        })
      ])
      ],


      as400VendorSku3: [new AttributeInputData(1, [
        new AttributeInputDataRow(1, "Vendor Code", [], "POVNDI", {
          name: false
        })
      ])
      ]


      ,
      productSize: [new AttributeInputData()],
      productSizeV: [new AttributeInputData()],
      productSize1: [new AttributeInputData()],
      productSize1V: [new AttributeInputData()],
      productSizeT: [new AttributeInputData()],
      productSize1T: [new AttributeInputData()],
      productSize2: [new AttributeInputData()],
      productSize10: [new AttributeInputData()],
      productSize11: [new AttributeInputData()],
      pricingData: [],
    });
  }

  pricingCols: any[];

  ngOnInit(): void {
    this.pricingCols = pricingCols;
    //if edit - call api to get the data
    let edit = false;
    if (edit) {
      this.getApiData();
    }
  }

  // handle form submission
  handleSubmitForm() {
    if (this.productForm.valid) console.log(this.productForm.value);
    else alert("invalid data");
  }

  // get saved form data
  getApiData() {
    this.productForm.patchValue(data);
  }
}
