import { FormControl, FormGroup } from "@angular/forms";

export class AttributeInputData {
  //attr-input-data
  attributeId: number;
  nameValueCol: AttributeInputDataRow[] = [new AttributeInputDataRow()];

  constructor(attributeId?, nameValueCol?) {
    this.attributeId = attributeId;
    this.nameValueCol = nameValueCol || [new AttributeInputDataRow()];
  }
}

export class AttributeInputDataRow {
  //attr-input-datarow
  childAttributeId: any;
  name: string;
  value: any;
  tiedAttributeValue: string;
  disableProp: any;

  constructor(
    childAttributeId?: any,
    name?: string,
    value?: any,
    tiedAttributeValue?: string,
    disableProp?: any
  ) {
    this.childAttributeId = childAttributeId || 1;
    this.name = name || "";
    this.value = value || "";
    this.tiedAttributeValue = tiedAttributeValue || "";
    if (disableProp) this.disableProp = disableProp;
  }
}
export class AttributeInputControlRow {
  //attr--input-controlrow
  childAttributeId: number;
  name: FormControl;
  value: FormControl;
  tiedAttributeValue: FormControl;
  disableProp: FormGroup;
}
export class AttributeInputConfig {
  //attr-input-config
  id: number;
  datatype: string;
  rule: string;
  label: string;
}
export enum AttributeInputDataType { //attr-input-datatypes
  Text = "text",
  Number = "number",
  Boolean = "bool",
}
export enum AttributeInputRuleType { //attr-input-ruletypes
  Any = "any",
  Limit = "limit",
  Specefic = "specific",
}

export enum ColMandatory {
  All = 1,
  AtleastOne = 2,
  Optional = 3
}

export const data = {
  productSize: {
    attributeId: 1,
    nameValueCol: [
      {
        childAttributeId: 1,
        name: "Name 1",
        value: "Value 1",
        tiedAttributeValue: "X",
      },
      {
        childAttributeId: 2,
        name: "Name 2",
        value: "Value 2",
        tiedAttributeValue: "XX",
      },
      {
        childAttributeId: 3,
        name: "Name 3",
        value: "Value 3",
        tiedAttributeValue: "",
        disableProp: {
          name: true,
          value: true,
          tied: true,
        },
      },
    ],
  },
  productSize1: {
    attributeId: 1,
    nameValueCol: [
      {
        childAttributeId: 1,
        name: "IMPRINT SIZE",
        value: ["TEST", "Value 2"],
        tiedAttributeValue: "X",
      },
      {
        childAttributeId: 2,
        name: "IMPRINT COLORS",
        value: ["TEST ITEM"],
        tiedAttributeValue: "XY",
      },
    ],
  },
  productSize2: {
    attributeId: 1,
    nameValueCol: [
      {
        childAttributeId: 1,
        name: "GRAY",
        value: ["#919693", "430", "431"],
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 2,
        name: "ORANGE",
        value: ["#F96302"],
        tiedAttributeValue: "",
      },
    ],
  },
  pricingData: {
    data: [
      {
        id: 1,
        min: 100,
        max: 200,
        salePrice: 500,
        salePriceExpire: "03/12/2021",
        costPrice: 300,
        shippingCost: 50,
      },
      {
        id: 2,
        min: 201,
        max: 300,
        salePrice: 400,
        salePriceExpire: "05/12/2021",
        costPrice: 300,
        shippingCost: 50,
      },
    ],
  }
};

export const pricingCols = [
  {
    name: "Sale Price",
    field: "salePrice",
    salePrice: true,
    expiration: true,
    colMandatory: 1,
    precision: 2,
    type: "currency",
  },
  {
    name: "Cost Price", precision: 3, field: "costPrice", type: "currency",
    colMandatory: 2,
    toolTip: "Cost Price",
    expiration: true,
    salePrice: true,
  },
  {
    name: "Shipping Cost", field: "shippingCost", type: "currency",
    colMandatory: 3,
    expiration: true,
    quickFill: true,
    toolTip: "Shipping Cost"
  },
];

export const shpData = {
  productSize: {
    attributeId: 240235,
    nameValueCol: [
      {
        childAttributeId: 249436,
        name: "NYS Clothing Tax Exempt",
        value: "Y",
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 300793,
        name: "Characters Per Line",
        value: "35",
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 254762,
        name: "Imprint Type",
        value: "S",
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 277583,
        name: "Production Time",
        value: "7",
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 240262,
        name: "Item Data Complete",
        value: "Y",
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 240279,
        name: "Offered On Web",
        value: "Y",
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 254764,
        name: "Discontinue Date",
        value: "0",
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 254828,
        name: "Event Type",
        value: "GN",
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 249453,
        name: "Product Type",
        value: "TS",
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 240275,
        name: "Manufacturing Type",
        value: "20",
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 240273,
        name: "Pack Quantity",
        value: "0",
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 240272,
        name: "Pack UOM",
        value: "EA",
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 249451,
        name: "Rush Delivery",
        value: "N",
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 277573,
        name: "Vendor Production Time",
        value: "7",
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 254770,
        name: "Imprint Flag",
        value: "Y",
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 254771,
        name: "Imprint Color",
        value: "STANDARD",
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 254761,
        name: "Lines",
        value: "5",
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 240266,
        name: "Vendor Code",
        value: "ZZ9999",
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 249458,
        name: "Short Description",
        value: "T-SHIRT",
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 4593845,
        name: "Name",
        value: "GN19:GILDAN HIS/HERS SHR SLV T",
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 330891,
        name: "Vendor Sku",
        value: "G5000",
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 5089392,
        name: "Inital Key Code",
        value: "S2XA",
        tiedAttributeValue: "",
      },
    ],
  },
  productSize1: {
    attributeId: 240237,
    nameValueCol: [
      {
        childAttributeId: 330903,
        name: "5XLARGE",
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 4593904,
        name: "5XLARGE",
        value: "SHP2415",
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 2746626,
        name: "4XLARGE",
        value: "SHP-337",
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 249462,
        name: "4XLARGE",
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 2746620,
        name: "3XLARGE",
        value: "SHP-336",
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 249448,
        name: "3XLARGE",
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 2746619,
        name: "2XLARGE",
        value: "SH-1224",
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 249449,
        name: "2XLARGE",
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 2746618,
        name: "XLARGE",
        value: "SH-1223",
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 249429,
        name: "XLARGE",
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 2746615,
        name: "LARGE",
        value: "SH-1222",
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 249431,
        name: "LARGE",
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 249435,
        name: "SMALL",
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 249433,
        name: "MEDIUM",
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 2746614,
        name: "MEDIUM",
        value: "SH-1221",
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 2746623,
        name: "SMALL",
        value: "SH-1220",
        tiedAttributeValue: "",
      },
    ],
  },
  productSize2: {
    attributeId: 240241,
    nameValueCol: [
      {
        childAttributeId: 4593863,
        name: "SUPPLEMENTAL NOTES",
        value: "2017 HEALTH & WELLNESS S/B (P9053) PG.66  67PGS",
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 4593864,
        name: "PRODUCT COLORS",
        value: "SAFETY GREEN, SAPPHIRE, LIGHT BLUE, LIME, AZALEA, DAISY",
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 4593865,
        name: "SUPPLEMENTAL NOTES",
        value: "2017 SUMMER/SPRING APPAREL P9299 PG15  63PGS",
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 4593866,
        name: "SUPPLEMENTAL NOTES",
        value: "2017 FIRE PREVENTION (P9285) PG 66 72PGS",
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 391514,
        name: "PRODUCT DESCRIPTIVE TEXT",
        value: "MATERIAL: 100% COTTON",
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 4593867,
        name: "SUPPLEMENTAL NOTES",
        value: "2018 HEALTH & WELLNESS #4 (P9906) PG.76  80PGS.",
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 5032049,
        name: "SUPPLEMENTAL NOTES",
        value: "2020 IM PPE V2  P11549    PG.66   96 PGS.",
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 3659652,
        name: "SUPPLEMENTAL NOTES",
        value: "2017 FALL IME (P9513) PG.11  84 PGS",
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 435362,
        name: "PRODUCT DESCRIPTIVE TEXT",
        value: "GENDER: MALE",
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 4593868,
        name: "SUPPLEMENTAL NOTES",
        value: "2017 POSITIVE CLIMATE (P9229) PG.78 79 PAGES",
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 4593869,
        name: "SUPPLEMENTAL NOTES",
        value: "2019 IME SUMMER #3 (P10559)   PG.45  96 PGS.",
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 249439,
        name: "AS/400 Ignores T-Options",
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 4593870,
        name: "SUPPLEMENTAL NOTES",
        value: "2017 PUBLIC SAFETY (P9110) PG.55 63 PGS",
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 277578,
        name: "IMPRINT SIZE",
        value: "UP TO 5 LINES OR LOGO; 25 CHARACTERS PER LINE",
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 4593871,
        name: "SUPPLEMENTAL NOTES",
        value: "2018 IME FALL (P10050) PG.8  80 PGS.",
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 4593872,
        name: "SUPPLEMENTAL NOTES",
        value: "2017 RECOGNITION #1 (P915",
        tiedAttributeValue: "",
      },
    ],
  },
  productSize10: {
    attributeId: 260006,
    nameValueCol: [
      {
        childAttributeId: 438324,
        name: "NAVY",
        value: "289",
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 397543,
        name: "LIME",
        value: "#8CD600",
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 347335,
        name: "LIME",
        value: "375",
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 390312,
        name: "NAVY",
        value: "#002649",
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 327608,
        name: "BLUE",
        value: "301",
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 378761,
        name: "ATHLETIC GOLD",
        value: "#FFC61E",
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 331207,
        name: "ATHLETIC GOLD",
        value: "123",
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 2473750,
        name: "MULTI COLOR",
        value: "#CE1126#0038A8#8CD600#F9E814",
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 438343,
        name: "MULTI COLOR",
        value: "RNBOW",
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 438333,
        name: "GRAY",
        value: "431",
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 438297,
        name: "HOT PINK",
        value: "#CE007C",
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 2418652,
        name: "HOT PINK",
        value: "233",
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 327423,
        name: "ORANGE",
        value: "#F96302",
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 327426,
        name: "ORANGE",
        value: "165",
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 438311,
        name: "PALE PINK",
        value: "#F2AFC1",
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 2418651,
        name: "PALE PINK",
        value: "203",
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 316313,
        name: "GREEN",
        value: "#008751",
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 316274,
        name: "GREEN",
        value: "348",
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 443683,
        name: "YELLOW",
        value: "#F9E04C",
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 442892,
        name: "YELLOW",
        value: "115",
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 277523,
        name: "RED",
        value: "#CE1126",
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 438334,
        name: "GRAY",
        value: "#666D70",
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 277530,
        name: "RED",
        value: "186",
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 277529,
        name: "WHITE",
        value: "#FFFFFF",
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 277526,
        name: "BLACK",
        value: "BLACK",
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 277527,
        name: "BLACK",
        value: "#000000",
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 277528,
        name: "WHITE",
        value: "WHITE",
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 327609,
        name: "BLUE",
        value: "#005B99",
        tiedAttributeValue: "",
      },
    ],
  },
  productSize11: {
    attributeId: 260008,
    nameValueCol: [
      {
        childAttributeId: 377598,
        name: "FULL FRONT",
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 390315,
        name: "FULL BACK",
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 377599,
        name: "RIGHT CHEST",
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 377597,
        name: "LEFT CHEST",
        tiedAttributeValue: "",
      },
    ],
  },
  productSize12: {
    attributeId: 260009,
    nameValueCol: [
      {
        childAttributeId: 327610,
        name: "PERSONALIZATION ONLY",
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 277565,
        name: "STOCK LOGO + PERSONALIZATION",
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 327954,
        name: "STOCK LOGO + PERSONALIZATION",
        value: "EACH",
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 434673,
        name: "STOCK LOGO + PERSONALIZATION",
        value: "2.00",
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 4593844,
        name: "STOCK LOGO ONLY.",
        value: "3U",
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 4593915,
        name: "STOCK LOGO ONLY.",
        value: "EACH",
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 4593922,
        name: "STOCK LOGO ONLY.",
        value: "2.00",
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 4593824,
        name: "STOCK LOGO ONLY.",
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 327979,
        name: "STOCK LOGO + PERSONALIZATION",
        value: "3U",
        tiedAttributeValue: "",
      },
    ],
  },
  productSize13: {
    attributeId: 277483,
    nameValueCol: [
      {
        childAttributeId: 277522,
        name: "ORANGE",
        value: ["165", "#F96302"],
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 343335,
        name: "PURPLE",
        value: ["268", "#4F2170"],
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 383136,
        name: "ROYAL",
        value: ["#0038A8", "286"],
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 2419885,
        name: "RED",
        value: ["#D81E05", "485"],
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 377882,
        name: "NAVY",
        value: ["#002649", "289"],
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 434806,
        name: "LIGHT PINK",
        value: ["203", "#F2AFC1"],
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 298177,
        name: "BLACK",
        value: ["BLACK", "#000000"],
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 2746629,
        name: "YELLOW/DAISY",
        value: ["122", "#FCD856"],
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 4593903,
        name: "CAROLINA BLUE",
        value: "#75B2DD",
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 2746633,
        name: "GOLD",
        value: "#FFC61E",
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 434818,
        name: "DARK HEATHER",
        value: "445",
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 3605588,
        name: "IRISH GREEN",
        value: ["#00B760", "354"],
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 4593910,
        name: "AZALEA",
        value: ["#F6A5BE", "197"],
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 4593912,
        name: "SAFETY ORANGE",
        value: ["#F74902", "172"],
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 4593914,
        name: "SAFETY GREEN",
        value: ["#D6E542", "380"],
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 2422408,
        name: "SAPPHIRE",
        value: ["#0054A0", "2945"],
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 453971,
        name: "LIME GREEN",
        value: ["#7FBA00", "376"],
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 458115,
        name: "GOLD",
        value: "123",
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 2476913,
        name: "SPORT GREY",
        value: ["402", "#AFA593"],
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 4593909,
        name: "HELICONIA PINK",
        value: ["#FC70BA", "231"],
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 456044,
        name: "LIGHT BLUE",
        value: ["#93DDDB", "318"],
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 449376,
        name: "MAROON",
        value: ["#8C2633", "202"],
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 2746624,
        name: "LIGHT GRAY (ASH) (BIRCH)",
        value: ["#ADAFAA", "429"],
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 4116845,
        name: "CAROLINA BLUE",
        value: "292",
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 458632,
        name: "DARK HEATHER",
        value: "#565959",
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 4593919,
        name: "SAFETY PINK",
        value: "183",
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 396300,
        name: "CHARCOAL",
        value: ["444", "#898E8C"],
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 4593911,
        name: "SAFETY PINK",
        value: "#FC8C99",
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 378914,
        name: "WHITE",
        value: ["WHITE", "#FFFFFF"],
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 634154,
        name: "FOREST GREEN",
        value: ["343", "#00563F"],
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 2705910,
        name: "VIOLET",
        value: ["526", "#68217A"],
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 4593921,
        name: "TROPICAL BLUE",
        value: ["313", "#0099B5"],
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 4593917,
        name: "HEATHER RADIANT ORCHID",
        value: ["183", "#FC8C99"],
        tiedAttributeValue: "",
      },
      {
        childAttributeId: 383115,
        name: "KELLY GREEN",
        value: ["355", "#009E49"],
        tiedAttributeValue: "",
      },
    ],
  },
  pricingData: {
    data: [
      {
        id: 1,
        min: 100,
        max: 200,
        salePrice: 500,
        costPrice: 300,
        shippingCost: 50,
        shippingCostExpire: "04/12/2021",
      },
      {
        id: 2,
        min: 201,
        max: 300,
        salePrice: 400,
        costPrice: 300,
        shippingCost: 50,
        shippingCostExpire: "06/12/2021",
      },
    ],
  }
};

export const shpPricingCols = [
  {
    name: "Sale Price",
    field: "salePrice",
    salePrice: true,
    colMandatory: 1,
    precesion: 0,
    type: "currency",
  },
  {
    name: "Cost Price", precesion: 3, field: "costPrice", type: "currency",
    colMandatory: 2,
    toolTip: "Cost Price",
    salePrice: true,
  },
  {
    name: "Shipping Cost", field: "shippingCost", type: "currency",
    colMandatory: 3,
    expiration: true,
    quickFill: true,
    toolTip: "Shipping Cost"
  },
];

export const itpPricingCols = [
  {
    name: "Sale Price",
    field: "salePrice",
    salePrice: true,
    expiration: true,
    colMandatory: 1,
    quickFill: true,
    precesion: 0,
    type: "currency",
  },
  {
    name: "Cost Price", precesion: 3, field: "costPrice", type: "currency",
    colMandatory: 2,
    expiration: true,
    toolTip: "Cost Price",
    salePrice: true,
  },
  {
    name: "Shipping Cost", field: "shippingCost", type: "currency",
    colMandatory: 3,
    quickFill: true,
    toolTip: "Shipping Cost"
  },
];

