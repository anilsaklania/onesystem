import { Component, OnInit } from '@angular/core';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng-lts/dynamicdialog';
import { OdataService } from '../../../services/odata.service';
import { UtilitiesService } from '../../../services/shared/utilities-service';
import { ProductService } from '../../../services/typescript-angular-client';

@Component({
  selector: 'app-product-field-config',
  templateUrl: './product-field-config.component.html',
  providers: [UtilitiesService]
})
/** ProductFieldConfig component*/
export class ProductFieldConfigComponent implements OnInit {
  tiedOptions: string;
  result: any = {
    label: null,
    tiedOptions: null,
    allowNewRow: false,
    allowMultipleValues: false,
    regex: null,
    standAlone: false,
    ignoreParent: false,
    nameOnly: false,
    valueCapitalize: true,
    orientation: 'V',
    ignoreSpecificValue: false,
    resolution: null
  };
  constructor(public ref: DynamicDialogRef, public config: DynamicDialogConfig, private product: ProductService,
    private odata: OdataService, private utils: UtilitiesService) {

  }
  ngOnInit(): void {
    let attrId = this.config.data.id;
    this.odata.query("product/attribute", `$expand=Audit&$filter=Id eq ${attrId}`).subscribe(x => {
      if (x.length == 0)
        return;
      let attr = x[0];
      if (attr.audit && attr.audit.value) {
        let tmp = JSON.parse(attr.audit.value);
        this.utils.objectKey(this.result).forEach(d => {
          if (tmp[d])
            if (d == "tiedOptions")
              this.result.tmpTiedOptions = (<string[]>tmp[d]).join(',');
            else
              this.result[d] = tmp[d];
        });
        this.result = { ...this.result };
      }
    });
  }
  update(evt: any, field: string) {
    if(evt)
    this.result.tiedOptions = (<string>evt).split(',');
  }

  save() {
    if (this.result.tiedOptions) {
      if ( (typeof this.result.tiedOptions) == "string")
        this.result.tiedOptions = [this.result.tiedOptions];
    }
    this.product.apiProductUpdateAttributePost({
      attribute: {
        id: this.config.data.id
      },
      type: "AUDIT",
      value: JSON.stringify(this.result)
    }).subscribe();
    this.ref.close(); 
  }
}
