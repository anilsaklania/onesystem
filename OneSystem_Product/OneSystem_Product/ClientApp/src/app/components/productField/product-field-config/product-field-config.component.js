"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProductFieldConfigComponent = void 0;
const core_1 = require("@angular/core");
const dynamicdialog_1 = require("primeng-lts/dynamicdialog");
const odata_service_1 = require("../../../services/odata.service");
const utilities_service_1 = require("../../../services/shared/utilities-service");
const typescript_angular_client_1 = require("../../../services/typescript-angular-client");
let ProductFieldConfigComponent = 
/** ProductFieldConfig component*/
class ProductFieldConfigComponent {
    constructor(ref, config, product, odata, utils) {
        this.ref = ref;
        this.config = config;
        this.product = product;
        this.odata = odata;
        this.utils = utils;
        this.result = {
            label: null,
            tiedOptions: null,
            allowNewRow: false,
            allowMultipleValues: false,
            regex: null,
            standAlone: false,
            ignoreParent: false,
            nameOnly: false,
            valueCapitalize: true,
            orientation: 'V',
            ignoreSpecificValue: false,
            resolution: null
        };
    }
    ngOnInit() {
        let attrId = this.config.data.id;
        this.odata.query("product/attribute", `$expand=Audit&$filter=Id eq ${attrId}`).subscribe(x => {
            if (x.length == 0)
                return;
            let attr = x[0];
            if (attr.audit && attr.audit.value) {
                let tmp = JSON.parse(attr.audit.value);
                this.utils.objectKey(this.result).forEach(d => {
                    if (tmp[d])
                        if (d == "tiedOptions")
                            this.result.tmpTiedOptions = tmp[d].join(',');
                        else
                            this.result[d] = tmp[d];
                });
                this.result = Object.assign({}, this.result);
            }
        });
    }
    update(evt, field) {
        if (evt)
            this.result.tiedOptions = evt.split(',');
    }
    save() {
        if (this.result.tiedOptions) {
            if ((typeof this.result.tiedOptions) == "string")
                this.result.tiedOptions = [this.result.tiedOptions];
        }
        this.product.apiProductUpdateAttributePost({
            attribute: {
                id: this.config.data.id
            },
            type: "AUDIT",
            value: JSON.stringify(this.result)
        }).subscribe();
        this.ref.close();
    }
};
ProductFieldConfigComponent = __decorate([
    core_1.Component({
        selector: 'app-product-field-config',
        templateUrl: './product-field-config.component.html',
        providers: [utilities_service_1.UtilitiesService]
    })
    /** ProductFieldConfig component*/
    ,
    __metadata("design:paramtypes", [dynamicdialog_1.DynamicDialogRef, dynamicdialog_1.DynamicDialogConfig, typescript_angular_client_1.ProductService,
        odata_service_1.OdataService, utilities_service_1.UtilitiesService])
], ProductFieldConfigComponent);
exports.ProductFieldConfigComponent = ProductFieldConfigComponent;
//# sourceMappingURL=product-field-config.component.js.map