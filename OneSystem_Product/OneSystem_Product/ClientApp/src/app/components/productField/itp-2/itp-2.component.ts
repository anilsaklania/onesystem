import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup } from "@angular/forms";
import {
  AttributeInputData,
  AttributeInputDataRow,
  data,
  itpPricingCols,
} from "../models/product-attr-input.models";

@Component({
  selector: "app-itp-2",
  templateUrl: "./itp-2.component.html",
  styleUrls: ["./itp-2.component.css"],
})
export class ITP2Component implements OnInit {
  public productForm: FormGroup;
  constructor(private formBuilder: FormBuilder) {
    this.productForm = this.formBuilder.group({
      productSize: [
        new AttributeInputData(1, [
          new AttributeInputDataRow(1, "PRO BLUE", ["BLACK"]),
        ]),
      ],
      productSize1: [
        new AttributeInputData(2, [
          new AttributeInputDataRow(1, "IMPRINT ON BACK", "TEST", "", {
            name: true,
          }),
        ]),
      ],
      productSize2: [new AttributeInputData()],
      productSize10: [
        new AttributeInputData(4, [
          new AttributeInputDataRow(1, "PACKAGING", "VALUE"),
        ]),
      ],
      productSize11: [new AttributeInputData()],
      pricingData: [],
    });
  }

  pricingCols: any[];

  ngOnInit(): void {
    this.pricingCols = itpPricingCols;
    //if edit - call api to get the data
    let edit = false;
    if (edit) {
      this.getApiData();
    }
  }

  // handle form submission
  handleSubmitForm() {
    if (this.productForm.valid) console.log(this.productForm.value);
    else alert("invalid data");
  }

  // get saved form data
  getApiData() {
    this.productForm.patchValue(data);
  }
}
