import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ITP2Component } from './itp2.component';

describe('ITP2Component', () => {
  let component: ITP2Component;
  let fixture: ComponentFixture<ITP2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ITP2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ITP2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
