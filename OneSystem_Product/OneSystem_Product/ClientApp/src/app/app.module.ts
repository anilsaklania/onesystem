import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { CommonModule } from '@angular/common';
import { ModuleWithProviders, NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { HttpClientModule } from "@angular/common/http";
import { RouterModule } from "@angular/router";
import { Title } from "@angular/platform-browser";
import { ReactiveFormsModule } from "@angular/forms";
import { environment } from "../environments/environment";

//general
import { AppComponent } from "./app.component";

// Primeng Imports
import { CardModule } from "primeng-lts/card";
import { SidebarModule } from "primeng-lts/sidebar";
import { TabViewModule } from "primeng-lts/tabview";
import { TabMenuModule } from "primeng-lts/tabmenu";
import { MenubarModule } from "primeng-lts/menubar";
import { MenuModule } from "primeng-lts/menu";
import { PanelMenuModule } from "primeng-lts/panelmenu";
import { ToastModule } from "primeng-lts/toast";
import { TableModule } from "primeng-lts/table";
import { MultiSelectModule } from "primeng-lts/multiselect";
import { DropdownModule } from "primeng-lts/dropdown";
import { DragDropModule } from "primeng-lts/dragdrop";
import { DialogModule } from "primeng-lts/dialog";
import { CalendarModule } from "primeng-lts/calendar";
import { AutoCompleteModule } from "primeng-lts/autocomplete";
import { CheckboxModule } from "primeng-lts/checkbox";
import { ScrollPanelModule } from "primeng-lts/scrollpanel";
import { CarouselModule } from "primeng-lts/carousel";
import { ContextMenuModule } from "primeng-lts/contextmenu";
import { PickListModule } from "primeng-lts/picklist";
import { OrderListModule } from "primeng-lts/orderlist";
import { StepsModule } from "primeng-lts/steps";
import { BlockUIModule } from "primeng-lts/blockui";
import { InputSwitchModule } from "primeng-lts/inputswitch";
import { KeyFilterModule } from "primeng-lts/keyfilter";
import { AccordionModule } from "primeng-lts/accordion";
import { GalleriaModule } from "primeng-lts/galleria";
import { TooltipModule } from "primeng-lts/tooltip";
import { OverlayPanelModule } from "primeng-lts/overlaypanel";
import { SelectButtonModule } from "primeng-lts/selectbutton";
import { TreeTableModule } from "primeng-lts/treetable";
import { DataViewModule } from "primeng-lts/dataview";
import { InputTextModule } from "primeng-lts/inputtext";
import { ColorPickerModule } from "primeng-lts/colorpicker";
import { FileUploadModule } from "primeng-lts/fileupload";
import { FieldsetModule } from "primeng-lts/fieldset";
import { OrganizationChartModule } from "primeng-lts/organizationchart";
import { ConfirmDialogModule } from "primeng-lts/confirmdialog";
import { ToolbarModule } from "primeng-lts/toolbar";
import { DialogService, DynamicDialogConfig, DynamicDialogModule, DynamicDialogRef } from "primeng-lts/dynamicdialog"
import { EditorModule } from 'primeng-lts/editor';
import { NgxShimmerLoadingModule } from "ngx-shimmer-loading";
import { NgxJsonViewerModule } from "ngx-json-viewer";
import { InputNumberModule } from 'primeng-lts/inputnumber';
import { RippleModule } from "primeng-lts/ripple";

import { NotificationService } from "./services/shared/notification-service";
import { DisplayService } from "./services/shared/display.service";


import { ConfirmationService, MessageService } from "primeng-lts/api";
import { ApiModule } from "./services/typescript-angular-client/api.module";
import {  Configuration,  ConfigurationParameters,} from "./services/typescript-angular-client";

import { OdataService } from "./services/odata.service";

import { ProductAttrInputComponent } from "./components/productField/product-attr-input/product-attr-input.component";
import { ProductFormComponent } from "./components/productField/product-form/product-form.component";
import { AttributeInputService } from "./components/productField/service/product-attr-input.service";
import { ITP2Component } from "./components/productField/itp-2/itp-2.component";
import { SHP735Component } from "./components/productField/shp735/shp735.component";
import { PricingTableComponent } from "./components/pricingTable/pricing-table/pricing-table.component";
import { AddRowDirective } from "./components/pricingTable/addRowDirective";
import { ProductFieldConfigComponent } from "./components/productField/product-field-config/product-field-config.component";

@NgModule({
  declarations: [
    AppComponent,
    ProductFieldConfigComponent,
    ProductAttrInputComponent,
    ProductFormComponent,
    ITP2Component,
    SHP735Component,
    PricingTableComponent,
    AddRowDirective,
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: "ng-cli-universal" }),
    BrowserAnimationsModule,
    HttpClientModule,
    InputNumberModule,
    FormsModule,
    CommonModule,
    NgxJsonViewerModule,
    ReactiveFormsModule,
    RouterModule.forRoot([
      {
        path: "",
        pathMatch: "full",
        redirectTo: "product-form",
        data: { title: "Landing" },
      },
      {
        path: "product-form",
        component: ProductFormComponent,
        data: { title: "Product Form" },
      },
     
      //{
      //  path: "forms/product",
      //  component: ProductFormPageComponent,
      //  data: { title: "Forms - Product" },
      //},
      
      {
        path: "itp-2",
        component: ITP2Component,
        data: { title: "ITP-2 " },
      },
      {
        path: "shp-735",
        component: SHP735Component,
        data: { title: "SHP-735" },
      },
      {
        path: "pricing-table",
        component: PricingTableComponent,
        data: { title: "Pricing Table" },
      },
    ]),
    CardModule,
    SidebarModule,
    TabViewModule,
    MenuModule,
    CarouselModule,
    StepsModule,
    MenubarModule,
    PanelMenuModule,
    ToastModule,
    AutoCompleteModule,
    ScrollPanelModule,
    OrderListModule,
    InputTextModule,
    TableModule,
    MultiSelectModule,
    TooltipModule,
    DragDropModule,
    PickListModule,
    InputSwitchModule,
    DialogModule,
    DropdownModule,
    CalendarModule,
    CheckboxModule,
    ContextMenuModule,
    BlockUIModule,
    OrganizationChartModule,
    KeyFilterModule,
    AccordionModule,
    GalleriaModule,
    TreeTableModule,
    TooltipModule,
    OverlayPanelModule,
    SelectButtonModule,
    DataViewModule,
    ColorPickerModule,
    FileUploadModule,
    FieldsetModule,
    TabMenuModule,
    NgxShimmerLoadingModule,
    ToolbarModule, RippleModule, EditorModule,
    ConfirmDialogModule, DynamicDialogModule,
    ApiModule.forRoot(apiConfigFactory),
  ],
  providers: [
    DisplayService,
    MessageService,
    NotificationService,
    Title,
    OdataService,
    AttributeInputService,
    ConfirmationService, DialogService, DynamicDialogRef, DynamicDialogConfig
  ],
  bootstrap: [AppComponent],
  entryComponents: [ProductFormComponent]
})
export class AppModule {
}
declare module "@angular/core" {
  interface ModuleWithProviders<T = any> {
    ngModule: Type<T>;
    providers?: Provider[];
  }
}
export function apiConfigFactory(): Configuration {
  const params: ConfigurationParameters = {
    basePath: `${environment.clientRoot}`,
  };

  let res = new Configuration(params);
  return res;
}
