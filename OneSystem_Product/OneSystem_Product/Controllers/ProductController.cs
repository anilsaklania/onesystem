﻿
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.OData.Query;
using OneSystem.Application.Products.Queries.GetProductAttributesSearch;
using OneSystem.Domain.Entities;
using OneSystem.Persistence.Entities;

namespace OneSystem_Product.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ProductController : BaseController
    {
        private readonly OneSystemDbContext context;
        public ProductController(OneSystemDbContext context)
        {
            this.context = context;
        }
        //[HttpGet]
        //[EnableQuery(MaxExpansionDepth = 0)]
        //[Route("/odata/product")]
        //public IEnumerable<Product> Get()
        //{
        //    return this.context.Products;
        //}
        //[HttpGet]
        //[EnableQuery]
        //[Route("/odata/product/statictypereference")]
        //public IEnumerable<ProductStaticTypeReference> GetStaticTypeReferences()
        //{
        //    return this.context.ProductStaticTypeReferences;
        //}
        [HttpGet]
        [EnableQuery]
        [Route("/odata/product/attribute")]
        public IEnumerable<ProductAttribute> GetAttributes()
        {
            return this.context.ProductAttributes;
        }
        //[HttpGet]
        //[EnableQuery]
        //[Route("/odata/product/sku_prefixsuffix")]
        //public IEnumerable<ProductSkuPrefixSuffix> GetSkuPrefixSuffix()
        //{
        //    return this.context.ProductSkuPrefixSuffixes;
        //}        

        [HttpPost]
        [Route("GetProductAttributesSearch")]
        public async Task<ActionResult<ProductAttribute[]>> GetProductAttributesSearch([FromBody] GetProductAttributesSearchQuery query)
        {
            return Ok(await Mediator.Send(query));
        }
        
    }
}
