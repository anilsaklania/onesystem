﻿using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace OneSystem_Product.Controllers
{
    //[ApiController]
    ////[Route("api/[controller]/[action]")]
    //[Route("[controller]")]
    public abstract class BaseController : ControllerBase
    {
        private IMediator _mediator;
        protected IMediator Mediator => _mediator ?? (_mediator = HttpContext.RequestServices.GetService<IMediator>());
    }
}
