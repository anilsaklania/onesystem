﻿using System;
using System.Collections.Generic;

namespace OneSystem.Domain.Entities
{
    public partial class ProductAttribute
    {
        public ProductAttribute()
        {
            InverseParentAttribute = new HashSet<ProductAttribute>();
        }

        public long Id { get; set; }
        public string Name { get; set; } = null!;
        public string? Type { get; set; }
        public string? Value { get; set; }
        public long? ParentAttributeId { get; set; }
        public string? Hiearchy { get; set; }
        public long? AuditId { get; set; }
        public DateTime? LastModifiedDate { get; set; }
        public DateTime? EntryDate { get; set; }
        public long? LastUserId { get; set; }
        public long? UserId { get; set; }
        public long? ColorId { get; set; }
        public string? LastUsername { get; set; }
        public string? Username { get; set; }
        public string? HiearchyString { get; set; }

        public virtual ProductProductAttributeAudit? Audit { get; set; }
        public virtual ProductColor? Color { get; set; }
        public virtual ProductAttribute? ParentAttribute { get; set; }
        public virtual ICollection<ProductAttribute> InverseParentAttribute { get; set; }
    }
}
