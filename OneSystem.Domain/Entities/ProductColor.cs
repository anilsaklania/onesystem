﻿using System;
using System.Collections.Generic;

namespace OneSystem.Domain.Entities
{
    public partial class ProductColor
    {
        public ProductColor()
        {
            ProductAttributes = new HashSet<ProductAttribute>();
        }

        public long Id { get; set; }
        public string Pms { get; set; } = null!;
        public string Hex { get; set; } = null!;
        public DateTime LastModifiedDate { get; set; }
        public string? Name { get; set; }
        public bool? IsActive { get; set; }
        public string? SwatchId { get; set; }

        public virtual ICollection<ProductAttribute> ProductAttributes { get; set; }
    }
}
