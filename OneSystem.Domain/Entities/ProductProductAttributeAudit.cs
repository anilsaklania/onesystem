﻿using System;
using System.Collections.Generic;

namespace OneSystem.Domain.Entities
{
    public partial class ProductProductAttributeAudit
    {
        public ProductProductAttributeAudit()
        {
            ProductAttributes = new HashSet<ProductAttribute>();
        }

        public long Id { get; set; }
        public string? Type { get; set; }
        public DateTime EntryDate { get; set; }
        public long UserId { get; set; }
        public string? Value { get; set; }
        public string? DataType { get; set; }
        public DateTime? LastValidationDate { get; set; }

        public virtual ICollection<ProductAttribute> ProductAttributes { get; set; }
    }
}
