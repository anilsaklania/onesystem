﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace OneSystem.Domain.Entities.OneSystemDb
{
    public class ProductProduct
    {
        public long ParentProductId { get; set; }
        public long ChildProductId { get; set; }

        public decimal UOM { get; set; }
        public string Hiearchy { get; set; }
        [JsonIgnore]
        public virtual Product ParentProduct { get; set; }
        [JsonIgnore]
        public virtual Product ChildProduct { get; set; }
    }
}
