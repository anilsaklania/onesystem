﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace OneSystem.Domain.Entities.OneSystemDb
{
    public class ProductPrice
    {
        public ProductPrice()
        {
            ProductPrices = new HashSet<ProductProductPrice>(); 
        }
        public long? Id { get; set; }
        public string Type { get; set; }
        public decimal Value { get; set; }
        public decimal? Discount { get; set; }
        public string DiscountType { get; set; }
        public int? Quantity { get; set; }
        public string TypeMeta { get; set; }
        [JsonIgnore]
        public virtual ICollection<ProductProductPrice> ProductPrices { get; set; }
        public override bool Equals(object obj)
        {
            var y = obj as ProductPrice;
            if (y == null)
                return false;
            return this.Type == y.Type && this.Value == y.Value && this.Quantity == y.Quantity && this.DiscountType == y.DiscountType && this.Discount == y.Discount && this.TypeMeta == y.TypeMeta;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hash = 37;
                hash = hash * 23 + this.Type.GetHashCode();
                hash = hash * 23 + this.Value.GetHashCode();
                if (this.Quantity.HasValue)
                    hash = hash * 23 + this.Quantity.GetHashCode();
                if (!string.IsNullOrWhiteSpace(DiscountType))
                    hash = hash * 23 + this.DiscountType.GetHashCode();
                if (this.Discount.HasValue)
                    hash = hash * 23 + this.Discount.GetHashCode();
                if (!string.IsNullOrWhiteSpace(this.TypeMeta))
                    hash = hash * 23 + this.TypeMeta.GetHashCode();
                return hash;
            }
        }
    }
}
