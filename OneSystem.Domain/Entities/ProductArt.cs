﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OneSystem.Domain.Entities
{
    public class ProductArt
    {
        public long? Id { get; set; }
        public string ParentCategory { get; set; }
        public string ChildCategory { get; set; }
        public string Type { get; set; }
        public bool CanScreenPrint { get; set; }
        public bool IsDigitized { get; set; }
        public int Colors { get; set; }
        public string Name { get; set; }
        public string ImageUrl { get; set; }
        public string SvgUrl { get; set; }
        public string MetaId { get; set; }
        public string Keywords { get; set; }
    }
}
