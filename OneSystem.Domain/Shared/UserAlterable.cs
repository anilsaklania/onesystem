﻿using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;

namespace OneSystem.Domain.Shared
{
    public class UserAlterable
    {
        public DateTime? LastModifiedDate { get; set; }
        public DateTime? EntryDate { get; set; }
        public long? LastUserId { get; set; }
        public string LastUsername { get; set; }
        public long? UserId { get; set; }
        public string Username { get; set; }
    }
}
