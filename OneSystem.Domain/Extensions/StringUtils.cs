﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OneSystem.Domain.Extensions
{
    public static partial class StringUtils
    {
        public static string Between(this string x, string start, string end )
        {
            int pFrom = x.IndexOf(start) + start.Length;
            int pTo = x.LastIndexOf(end);

            return x.Substring(pFrom, pTo - pFrom);
        }
    }
}
