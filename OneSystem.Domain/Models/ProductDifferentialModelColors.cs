﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OneSystem.Domain.Models
{
    public class ProductDifferentialModelColors
    {
        public DateTime MaxEntryDate { get; set; }
        public IEnumerable<ProductDifferentialColorsValuePair> Values { get; set; }
    }
    public class ProductDifferentialColorsValuePair
    {
        public string PMS { get; set; }
        public string Hex { get; set; }
    }
}
