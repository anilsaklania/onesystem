﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OneSystem.Domain.Models
{
    public class ComponentStructureModel
    {
        public string Sku { get; set; }
        public string Type { get; set; }
        public decimal UOM { get; set; }
        public ComponentStructureModel(string value, string type)
        {
            string[] values = value.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
            decimal tmpOut = 0;
            if (decimal.TryParse(values[0].TrimStart('0'), out tmpOut))
            {
                Sku = values[1].Trim().ToUpper();
                UOM = tmpOut;
                Type = type;
            }
        }
        public ComponentStructureModel()
        {

        }
        public static ComponentStructureModel TryParse(string value, string type)
        {
            string[] values = value.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
            decimal tmpOut = 0;
            if (decimal.TryParse(values[0].TrimStart('0'), out tmpOut))
            {
                return new ComponentStructureModel
                {
                    Sku = values[1].Trim().ToUpper(),
                    UOM = tmpOut,
                    Type = type
                };
            }
            return null;
        }
    }
}
