﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OneSystem.Domain.Models
{
    public class ProductDifferentialModel
    {
        public DateTime MaxEntryDate { get; set; }
        public IEnumerable<ProductDifferentialValuePair> Values { get; set; }
    }
    public class ProductDifferentialValuePair
    {
        public string Type { get; set; }
        public string Sku { get; set; }
        public string User { get; set; }
    }
}
