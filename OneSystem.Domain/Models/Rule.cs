﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
//using OneSystem.Domain.Entities.OneSystemDb;
using OneSystem.Domain.Extensions;

namespace OneSystem.Domain.Models
{
    public class Rule
    {
        public string MemberName { get; set; }
        public string Operator { get; set; }
        public string TargetValue { get; set; }
        public int? Group { get; set; }
        public string? Type { get; set; }
        public string? GroupType { get; set; }

        public static Func<T, bool> CompileRule<T>(Rule r)
        {
            var paramUser = Expression.Parameter(typeof(T));
            Expression expr = BuildExpr<T>(r, paramUser);
            // build a lambda function User->bool and compile it
            return Expression.Lambda<Func<T, bool>>(expr, paramUser).Compile();
        }
        public static Expression<Func<TSource, TResult>> GenerateSelector<TSource, TResult>(string propertyOrFieldName)
        {
            var parameter = Expression.Parameter(typeof(TSource));
            var body = Expression.Convert(
                // generate the appropriate member access
                Expression.PropertyOrField(parameter, propertyOrFieldName),
                typeof(TResult)
            );
            var expr = Expression.Lambda<Func<TSource, TResult>>(body, parameter);
            return expr;
        }
        public static Expression<Func<T, bool>> Where<T>(params Rule[] rules)
        {
            var paramUser = Expression.Parameter(typeof(T));
            List<KeyValuePair<string, Expression>> expressions = new List<KeyValuePair<string, Expression>>();
            Expression parentExpression = null;
            var grouppedRules = rules.GroupBy(x => x.Group);
            foreach (var group in grouppedRules)
            {
                Expression grouppedExpression = null;
                string lastGroupType = "AND";
                foreach (var item in group)
                {
                    if (!string.IsNullOrWhiteSpace(item.GroupType))
                        lastGroupType = item.GroupType;
                    Expression expr1 = BuildExpr<T>(item, paramUser);
                    if (grouppedExpression == null)
                        grouppedExpression = expr1;
                    else
                    {
                        if (string.IsNullOrWhiteSpace(item.Type) || item.Type == "AND")
                        {
                            grouppedExpression = Expression.AndAlso(grouppedExpression, expr1);
                        }
                        else
                        {
                            grouppedExpression = Expression.OrElse(grouppedExpression, expr1);
                        }
                    }
                }
                expressions.Add(new KeyValuePair<string, Expression>(lastGroupType, grouppedExpression));
            }
            foreach (var expr in expressions)
            {
                if (parentExpression == null)
                    parentExpression = expr.Value;
                else
                {
                    if (expr.Key == "AND")
                        parentExpression = Expression.AndAlso(parentExpression, expr.Value);
                    else
                        parentExpression = Expression.OrElse(parentExpression, expr.Value);

                }
            }
            return Expression.Lambda<Func<T, bool>>(parentExpression, paramUser);
        }

        public static Expression<Func<TOuter, bool>> Any<TOuter, TInner>(Expression<Func<TOuter, IEnumerable<TInner>>> innerSelector, Rule r) => innerSelector.Any(Where<TInner>(r));

        public static Expression<Func<TOuter, bool>> All<TOuter, TInner>(Expression<Func<TOuter, IEnumerable<TInner>>> innerSelector, Rule[] r) => innerSelector.Any(Where<TInner>(r));
        public static Expression<Func<TOuter, bool>> None<TOuter, TInner>(Expression<Func<TOuter, IEnumerable<TInner>>> innerSelector, Rule r) => innerSelector.NotAny(Where<TInner>(r));
        public static Expression BuildExpr<T>(Rule r, ParameterExpression param)
        {
            var left = MemberExpression.Property(param, r.MemberName);

            var tProp = typeof(T).GetProperty(r.MemberName).PropertyType;
            var nullable = Nullable.GetUnderlyingType(tProp);
            ExpressionType tBinary;
            // is the operator a known .NET operator?
            if (ExpressionType.TryParse(r.Operator, out tBinary))
            {
                var right = Expression.Convert(Expression.Constant(Convert.ChangeType(r.TargetValue, nullable ?? tProp, CultureInfo.InvariantCulture)), tProp);
                // use a binary operation, e.g. 'Equal' -> 'u.Age == 15'
                return Expression.MakeBinary(tBinary, left, right);
            }
            else
            {
                MethodInfo method = null;
                if (tProp == typeof(string))
                {
                    method = tProp.GetMethod(r.Operator, new[] { typeof(string) });
                }
                else
                    method = tProp.GetMethod(r.Operator);
               // var tParam = method.GetParameters()[0].ParameterType; 
                if(r.Operator=="HasValue")
                {
                    return Expression.NotEqual(left, Expression.Constant(null));
                }
                var right = Expression.Convert(Expression.Constant(Convert.ChangeType(r.TargetValue, nullable ?? tProp, CultureInfo.InvariantCulture)), tProp);
                // var right = Expression.Constant(Convert.ChangeType(r.TargetValue, tParam, CultureInfo.InvariantCulture));
                // use a method call, e.g. 'Contains' -> 'u.Tags.Contains(some_tag)'
                return Expression.Call(left, method, right);
            }
        }

        public static Object GetNestedPropertyValue(String name, Object obj)
        {
            foreach (String part in name.Split('.'))
            {
                if (obj == null) { return null; }

                Type type = obj.GetType();
                PropertyInfo info = type.GetProperty(part);
                if (info == null) { return null; }

                obj = info.GetValue(obj, null);
            }
            return obj;
        }

        public static string RulesToString(Rule[] rules)
        {
            StringBuilder sb = new StringBuilder();
            foreach(var x in rules.GroupBy(x => new { x.Group, x.GroupType }))
            {
                for(int i = 0; i < x.Count(); i++)
                {
                    var str = $"{x.ElementAt(i).MemberName} {x.ElementAt(i).Operator} {x.ElementAt(i).TargetValue}";
                    if (i != (x.Count() - 1))
                        str += $" { x.Key.GroupType ?? "and "}";
                    sb.Append(str);
                };
            };
            return sb.ToString();
        }
    }
}
