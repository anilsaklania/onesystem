﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OneSystem.Domain.Models
{
    public abstract class FilterableQuery
    {
        public FilterItem[] Filters { get; set; }
        public int? SortOrder { get; set; }
        public string SortBy { get; set; }
    }

    public class FilterItem
    {
        public string Name { get; set; }
        public string Value { get; set; }
        public string Type { get; set; }
    }
}
