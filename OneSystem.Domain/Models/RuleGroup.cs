﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OneSystem.Domain.Models
{
    public class RuleGroup
    {
        public string Type { get; set; }
        public RuleGroup[] Children { get; set; }
        public Rule[] Rules { get; set; }

        public bool IsTrue<T>(dynamic item) 
        {
            List<bool> successList = new List<bool>();
            if (Children != null)
                if(Type == "AND")
                successList.Add(Children.All(x => x.IsTrue(item)));
                else
                    successList.Add(Children.Any(x => x.IsTrue(item)));
            if (Rules != null)
            {
                var compiledRules = Rules.Select(r => Rule.CompileRule<T>(r)).ToList();
                if (Type == "AND")
                    successList.Add(compiledRules.All(x => x(item)));
                else
                    successList.Add(compiledRules.Any(x => x(item)));
            }
            return successList.All(x => x);
        }
    }
}
