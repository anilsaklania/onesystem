﻿using MediatR;
using OneSystem.Domain.Entities;
using OneSystem.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace OneSystem.Application.Products.Queries.GetProductAttributesSearch
{
    public class GetProductAttributesSearchQuery : IRequest<ProductAttribute[]>
    {
        public IEnumerable<Rule> Filter { get; set; }
        public string? DistinctBy { get; set; }
        public bool Distinct { get; set; } = false;
        public int? Start { get; set; }
        public int? Size { get; set; }
    }
}
