﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using OneSystem.Domain.Entities;
using OneSystem.Domain.Models;
using OneSystem.Persistence.Entities;

namespace OneSystem.Application.Products.Queries.GetProductAttributesSearch
{
    public class GetProductAttributesSearchQueryHandler : IRequestHandler<GetProductAttributesSearchQuery, ProductAttribute[]>
    {
        private readonly OneSystemDbContext _context;
        public GetProductAttributesSearchQueryHandler(OneSystemDbContext context)
        {
            this._context = context;
        }
        public async Task<ProductAttribute[]> Handle(GetProductAttributesSearchQuery request, CancellationToken cancellationToken)
        {
            IQueryable<ProductAttribute> entity = _context.ProductAttributes.Include(e => e.ParentAttribute).Include(e => e.Audit).Include(e => e.Color);
           
            if (request.Filter != null)
                foreach (var filter in request.Filter)
                    entity = entity.Where(Rule.Where<ProductAttribute>(filter));
            else
                entity = entity.Where(x => !x.ParentAttributeId.HasValue);
            
            var strRes = new List<string>();
            // for a distinct all values can return the same.... 
            if (request.Distinct)
                if (request.DistinctBy != null)
                    strRes = await entity.Select(Rule.GenerateSelector<ProductAttribute, string>(request.DistinctBy)).Distinct().ToListAsync();
                else
                    strRes = await entity.Select(Rule.GenerateSelector<ProductAttribute, string>(request.Filter.Last().MemberName)).Distinct().ToListAsync();
            ProductAttribute[] resAttrs = null;

            if (request.Distinct && strRes.Count > 0)
            {
                resAttrs = strRes.Select(x => new ProductAttribute
                {
                    Name = x,
                    Value = x,
                    Type = x
                }).ToArray();
            } 
            if (request.Size.HasValue && request.Start.HasValue)
            {
                if (resAttrs != null)
                {
                    if (resAttrs.Length >= (request.Start + request.Size))
                    {
                        resAttrs = resAttrs.Skip(request.Start.Value).Take(request.Size.Value).ToArray();
                    }
                    else
                    {
                        resAttrs = resAttrs.Skip(request.Start.Value).ToArray();
                    }
                }
                else
                {
                    entity = entity.OrderBy(x => x.Id).Skip(request.Start.Value).Take(request.Size.Value);
                }

            }
            return resAttrs != null ? resAttrs : await entity.ToArrayAsync();
        }
    }
}
