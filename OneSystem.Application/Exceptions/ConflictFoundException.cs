﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OneSystem.Application.Exceptions
{
    public class ConflictFoundException : Exception
    {
        public ConflictFoundException(string name, object key)
            : base($"Entity \"{name}\" ({key}) was found.")
        {
        }
    }
}
