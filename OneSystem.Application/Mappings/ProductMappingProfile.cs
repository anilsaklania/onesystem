﻿using AutoMapper;
using OneSystem.Domain.Entities.OneSystemDb;
using OneSystem.Domain.Services.AS400;
using System;
using System.Collections.Generic;
using System.Text;

namespace OneSystem.Application.Mappings
{
    public class ProductMappingProfile : Profile
    {
        public ProductMappingProfile()
        {
            CreateMap<AS400_ProductModel, Product>()
                .ForMember(x => x.Sku, f => f.MapFrom(y => y.Sku))
                .ForAllOtherMembers(x => x.Ignore());
            CreateMap<ProductHeaderModel, ProductAttribute>()
                .ForMember(x => x.Value, f => f.MapFrom(t => string.IsNullOrWhiteSpace(t.Value) ? null : t.Value))
                .ForMember(x => x.Name, f => f.MapFrom(t => ProductNoteNameConverter(t)))
                .ForMember(x => x.ParentAttribute, f => f.MapFrom(t => new ProductAttribute { Name = "AS400" }));
            CreateMap<ProductAttributeModel, ProductAttribute>()
                .ForMember(x => x.Name, f => f.MapFrom(t => string.IsNullOrWhiteSpace(t.Value) ? null : t.Value))
                .ForMember(x => x.Value, f => f.MapFrom(t => string.IsNullOrWhiteSpace(t.PMSCode) ? null : t.PMSCode))
                .ForMember(x => x.Type, f => f.MapFrom(t => t.Type=="C" || t.Type =="S" ? (!string.IsNullOrWhiteSpace(t.PMSCode)? "PMS" : null) : t.Type))
                .ForMember(x => x.ParentAttribute, f => f.MapFrom(t => new ProductAttribute { Name = t.Type == "C" ? "AS400 Specification Choice" : "AS400 Specification", Value = (string.IsNullOrWhiteSpace(t.Name) ? null : t.Name) }));
            CreateMap<ProductNoteModel, ProductAttribute>()
              .ForMember(x => x.Value, f => f.MapFrom(t => string.IsNullOrWhiteSpace(t.Value) ? null : t.Value))
              .ForMember(x => x.Name, f => f.MapFrom(t => string.IsNullOrWhiteSpace(t.Name) ? null : t.Name))
              .ForMember(x => x.Type, f => f.MapFrom(t => string.IsNullOrWhiteSpace(t.Type) ? null : t.Type))
              .ForMember(x => x.ParentAttribute, f => f.MapFrom(t => new ProductAttribute { Name = "AS400 Notes" }));
            // we can use .ReverseMap() but there's a alot of customizations here 

            CreateMap<Product, AS400_ProductModel>()
                .ForMember(x => x.Sku, f => f.MapFrom(y => y.Sku));
            CreateMap<ProductAttribute, ProductHeaderModel>()
                .ForMember(x => x.Line, f => f.Ignore());
            CreateMap<ProductAttribute, ProductNoteModel>()
                .ForMember(x => x.Line, f => f.Ignore());
            //Toption mapping custom handled.


        }
        private static object ProductNoteNameConverter(ProductHeaderModel obj)
        {
            switch (obj.Type)
            {
                case "PSITID":
                    return "Vendor Sku";
                case "POITDS":
                    return "Name";
                case "PSUCSN":
                    return "Succession Note";
                case "POVNDI":
                    return "Vendor Code";
                case "POITSH":
                    return "Short Description";
                case "PMAJLN":
                    return "Manufacturing Type";
                case "PMEDLN":
                    return "Product Type";
                case "PMINLN":
                    return "Event Type";
                case "PUOM":
                    return "Pack UOM";
                case "PPAKQ":
                    return "Pack Quantity";
                case "PRUSHD":
                    return "Rush Delivery";
                case "PSTRDL":
                    return "Vendor Production Time";
                case "PAVPRT":
                    return "Production Time";
                case "PIMPCT":
                    return "Imprint Type";
                case "PDNUED":
                    return "Discontinue Date";
                case "PIMPAV":
                    return "Imprint Flag";
                case "PNYSTE":
                    return "NYS Clothing Tax Exempt";
                case "PWEBYN":
                    return "Offered On Web";
                case "PITCMP":
                    return "Item Data Complete";
                case "PIMPCL":
                    return "Imprint Color";
                case "PIKCOD":
                    return "Inital Key Code";
                case "PIMPLN":
                    return "Lines";
                case "PCMPCD":
                    return "Component Code";
                case "PIMCPL":
                    return "Characters Per Line";
                default:
                    return null;
            }
        }
    }
}
