﻿using Microsoft.EntityFrameworkCore;
using OneSystem.Domain.CRM;
using System;
using System.Collections.Generic;
using System.Text;

namespace OneSystem.Persistence.MSSQL.OneSystemCRMDb
{
    public partial class OneSystemCRMDbContext : DbContext
    {

        public OneSystemCRMDbContext(DbContextOptions<OneSystemCRMDbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<LeadSource> LeadSources { get; set; }
        public virtual DbSet<LeadSourceMetadata> LeadSourceMetadatas { get; set; }
        public virtual DbSet<Lead> Leads { get; set; }
        public virtual DbSet<LeadAttribute> LeadAttributes { get; set; }
        public virtual DbSet<LeadAttributeValue> LeadAttributeValues { get; set; } 
        public virtual DbSet<AS400_Customer> AS400_Customers { get; set; }
        public virtual DbSet<AS400_CustomerDetail> AS400_CustomerDetails { get; set; }
        public virtual DbSet<Source> Sources { get; set; }
        public virtual DbSet<DataSource> DataSources { get; set; }
        public virtual DbSet<DataSource_Attribute> DataSource_Attributes { get; set; }
        public virtual DbSet<DataSourceAttribute> DataSourceAttributes { get; set; }
        public virtual DbSet<AS400_Order> AS400_Orders { get; set; }
        public virtual DbSet<AS400_OrderDetail> AS400_OrderDetails { get; set; }
        public virtual DbSet<AS400_OrderDetailDestination> AS400_OrderDetailDestination { get; set; }
        public virtual DbSet<AS400_OrderDetailStage> AS400_OrderDetailStages { get; set; }
        public virtual DbSet<AS400_OrderNote> AS400_OrderDetailNotes { get; set; }
        public virtual DbSet<AS400_OrderSummary> AS400_OrderSummary { get; set; }

        public virtual DbSet<Customer> Customers { get; set; }
        public virtual DbSet<CustomerReference> CustomerReferences { get; set; }
        public virtual DbSet<Reports> Reports { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {  
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(OneSystemCRMDbContext).Assembly);
        }
    }
}
