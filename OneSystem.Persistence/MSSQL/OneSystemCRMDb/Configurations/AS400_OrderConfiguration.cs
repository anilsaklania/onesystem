﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using OneSystem.Domain.CRM;
using System;
using System.Collections.Generic;
using System.Text;

namespace OneSystem.Persistence.MSSQL.OneSystemCRMDb.Configurations
{
    public class AS400_OrderConfiguration : IEntityTypeConfiguration<AS400_Order>
    {
        public void Configure(EntityTypeBuilder<AS400_Order> builder)
        {
            builder.ToTable("AS400_Order");
            builder.HasKey(e => e.OrderNumber);
            builder.Property(e => e.CompanyCode);
            builder.Property(e => e.CustomerNumber);
            builder.Property(e => e.CustomerSegmentNumber);
            builder.Property(e => e.CustomerBillTo);
            builder.Property(e => e.BillToSegmentNumber);
            builder.Property(e => e.EntryDate);
            builder.Property(e => e.LastAdjustment);
            builder.Property(e => e.TaxJurisdiction1);
            builder.Property(e => e.SalesPerson);
            builder.Property(e => e.Line);
            builder.Property(e => e.MultiCampaingn);
            builder.Property(e => e.SalesmanNumber);
            builder.Property(e => e.KeyCode);
            builder.Property(e => e.OrderSource);
            builder.Property(e => e.PaymentType);
            builder.Property(e => e.InitialUserID);
            builder.Property(e => e.LastUserID);
            builder.Property(e => e.EarlyDueDate);
            builder.Property(e => e.HBTRS1);
            builder.Property(e => e.HBTRSFILL);
            builder.Property(e => e.HBTRS2);
            builder.Property(e => e.ShippingChargesQuoted);
            builder.Property(e => e.CustomerPONumber);
            builder.Property(e => e.ReleaseStatus);
            builder.Property(e => e.ShippingStatus);
            builder.Property(e => e.InvoiceStatus);
            builder.Property(e => e.CancelledOrderStatus);
            builder.Property(e => e.NumberOfDetailLines);
            builder.Property(e => e.MemoPrintFlag);
            builder.Property(e => e.AcknowledgementPrinted);
            builder.Property(e => e.MemoOnContinuousForm);
            builder.Property(e => e.OrderType);
            builder.Property(e => e.InvoiceNumber);
            builder.Property(e => e.CampaignCode);
            builder.Property(e => e.ShippedDate);
            builder.Property(e => e.ShippedTime);
            builder.Property(e => e.SpecialHandling);
            builder.Property(e => e.IceSpoc);
            builder.Property(e => e.SoftAllocationNumber);
            builder.Property(e => e.LastModifiedDate);

            //builder.HasMany(e => e.Details)
            //    .WithOne(e => e.Order)
            //    .HasForeignKey(e => e.OrderNumber)
            //    .HasConstraintName("FK_AS400OrderDetail_AS400Order");
            ////builder.HasMany(e => e.Notes)
            ////    .WithOne(e => e.Order)
            ////    .HasForeignKey(e => e.OrderNumber)
            ////    .HasConstraintName("FK_AS400OrderNote_AS400Order");
            builder.HasMany(e => e.Summary)
                .WithOne(e => e.Order)
                .HasForeignKey(e => e.OrderNumber)
                .HasConstraintName("FK_AS400OrderSummmary_AS400Order");
        }
    }
}
