﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using OneSystem.Domain.CRM;
using System;
using System.Collections.Generic;
using System.Text;

namespace OneSystem.Persistence.MSSQL.OneSystemCRMDb.Configurations
{
    public class LeadConfiguration : IEntityTypeConfiguration<Lead>
    {
        public void Configure(EntityTypeBuilder<Lead> builder)
        {
            builder.ToTable("Lead");
            builder.HasKey(e => e.Id);
            builder.Property(e => e.EntryDate).IsRequired();
            builder.Property(e => e.IsCheckedInAPI);
            builder.HasMany(e => e.Values)
                .WithOne(e => e.Lead)
                .HasForeignKey(e => e.LeadId)
                .HasConstraintName("FK_LeadAttributeValue_Lead");
        }
    }
}
