﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using OneSystem.Domain.CRM;
using System;
using System.Collections.Generic;
using System.Text;

namespace OneSystem.Persistence.MSSQL.OneSystemCRMDb.Configurations
{
    public class LeadAttributeValueConfiguration : IEntityTypeConfiguration<LeadAttributeValue>
    {
        public void Configure(EntityTypeBuilder<LeadAttributeValue> builder)
        {
            builder.ToTable("LeadAttributeValue");
            builder.HasKey(e => new { e.AttributeId, e.LeadId });
            builder.Property(e => e.Value).HasMaxLength(4000);

        }
    }
}
