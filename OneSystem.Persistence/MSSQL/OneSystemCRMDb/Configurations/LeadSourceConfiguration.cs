﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using OneSystem.Domain.CRM;
using System;
using System.Collections.Generic;
using System.Text;

namespace OneSystem.Persistence.MSSQL.OneSystemCRMDb.Configurations
{
    public class LeadSourceConfiguration : IEntityTypeConfiguration<LeadSource>
    {
        public void Configure(EntityTypeBuilder<LeadSource> builder)
        {
            builder.ToTable("LeadSource");
            builder.HasKey(e => e.Id);
            builder.Property(e => e.Name).IsRequired();
            builder.Property(e => e.Type).IsRequired();
            builder.Property(e => e.LastModifiedDate).IsRequired();
            builder.Property(e => e.CreationDate).IsRequired();
            builder.HasMany(e => e.Leads)
                .WithOne(e => e.Source)
                .HasForeignKey(e => e.SourceId)
                .HasConstraintName("FK_Lead_LeadSource");
            builder.HasMany(e => e.Attributes)
                .WithOne(e => e.Source)
                .HasForeignKey(e => e.SourceId)
                .HasConstraintName("FK_LeadAttribute_LeadSource");
            builder.HasMany(e => e.Metadata)
                .WithOne(e => e.Lead)
                .HasForeignKey(e => e.SourceId)
                .HasConstraintName("FK_LeadSourceMetadata_LeadSource");
        }
    }
}
