﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using OneSystem.Domain.CRM;
using System;
using System.Collections.Generic;
using System.Text;

namespace OneSystem.Persistence.MSSQL.OneSystemCRMDb.Configurations
{
    public class CustomerReferenceConfiguration : IEntityTypeConfiguration<CustomerReference>
    {
        public void Configure(EntityTypeBuilder<CustomerReference> builder)
        {
            builder.ToTable("CustomerReference");
            builder.HasKey(e => new { e.CustomerId , e.Type, e.RefId});
            //builder.Property(e => e.Type);
            //builder.Property(e => e.RefId);
        }
    }
}
