﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using OneSystem.Domain.CRM;
using System;
using System.Collections.Generic;
using System.Text;

namespace OneSystem.Persistence.MSSQL.OneSystemCRMDb.Configurations
{
    public class DataSource_AttributeConfiguration : IEntityTypeConfiguration<DataSource_Attribute>
    {
        public void Configure(EntityTypeBuilder<DataSource_Attribute> builder)
        {
            builder.ToTable("DataSource_Attribute");
            //builder.HasKey(x => new { x.Id });
            builder.Property(x => x.Name);
            builder.Property(e => e.SourceId);
            builder.Property(x=>x.RefKey);
            builder.HasMany(x => x.DataSourceAttribute)
                .WithOne(x => x.DataSource_Attribute)
                .HasForeignKey(x => x.AttributeId)
                .HasConstraintName("FK_DataSourceAttribute_DataSource_Attribute");
                
        }
    }
}
