﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using OneSystem.Domain.CRM;
using System;
using System.Collections.Generic;
using System.Text;

namespace OneSystem.Persistence.MSSQL.OneSystemCRMDb.Configurations
{
    public class LeadAttributeConfiguration : IEntityTypeConfiguration<LeadAttribute>
    {
        public void Configure(EntityTypeBuilder<LeadAttribute> builder)
        {
            builder.ToTable("LeadAttribute");
            builder.Property(e => e.Name).IsRequired();
            builder.Property(e => e.SourceId).IsRequired();
            builder.Property(e => e.RefKey);
            builder.HasMany(e => e.Values)
                .WithOne(e => e.Attribute)
                .HasForeignKey(e => e.AttributeId)
                .HasConstraintName("FK_LeadAttributeValue_LeadAttribute");
        }
    }
}
