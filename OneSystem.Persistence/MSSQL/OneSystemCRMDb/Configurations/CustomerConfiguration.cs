﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using OneSystem.Domain.CRM;

namespace OneSystem.Persistence.MSSQL.OneSystemCRMDb.Configurations
{
    public class CustomerConfiguration : IEntityTypeConfiguration<Customer>
    {
        public void Configure(EntityTypeBuilder<Customer> builder)
        {
            builder.ToTable("Customer");
            builder.HasKey(e => e.Id);
            builder.Property(e => e.EntryDate);
            builder.Property(e => e.LastModifiedDate);
            builder.Property(e => e.LastMatchedDate);
            builder.HasMany(e => e.Customers)
                .WithOne(e => e.Source)
                .HasForeignKey(e => e.CustomerId)
                .HasConstraintName("FK_CustomerReference_Customer");
        }
    }
}
