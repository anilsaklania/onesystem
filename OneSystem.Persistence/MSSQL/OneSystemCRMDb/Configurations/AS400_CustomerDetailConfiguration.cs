﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using OneSystem.Domain.CRM;
using System;
using System.Collections.Generic;
using System.Text;

namespace OneSystem.Persistence.MSSQL.OneSystemCRMDb.Configurations
{
    public class AS400_CustomerDetailConfiguration : IEntityTypeConfiguration<AS400_CustomerDetail>
    {
        public void Configure(EntityTypeBuilder<AS400_CustomerDetail> builder)
        {
            builder.ToTable("AS400_CustomerDetail");
            //builder.HasKey(e => e.Id);
            builder.HasKey(e => e.CustomerNo);
            //builder.HasKey(e => e.CustomerNo);
            //builder.Property(e => e.CustomerNo).IsRequired();
            builder.Property(e => e.CompanyCode);
            builder.Property(e => e.Priority);
            builder.Property(e => e.NumberOfBeds);
            builder.Property(e => e.NumberOfNurses);
            builder.Property(e => e.NumberOfStudents);
            builder.Property(e => e.NumberOfVolunteers);
            builder.Property(e => e.Mascot);
            builder.Property(e => e.SchoolColor1);
            builder.Property(e => e.SchoolColor2);
            builder.Property(e => e.SICCode);
            builder.Property(e => e.Classification);
            builder.Property(e => e.OverrideAdress1);
            builder.Property(e => e.OverrideAdress2);
            builder.Property(e => e.OverrideCity);
            builder.Property(e => e.OverrideState);
            builder.Property(e => e.OverrideZip);
            builder.Property(e => e.CreationDate);
            builder.Property(e => e.BudgetEOYDate);
            builder.Property(e => e.SchoolEoyDate);
            builder.Property(e => e.GrandParentCode);
            builder.Property(e => e.ParentCode);
            builder.Property(e => e.ChildCode);
            builder.Property(e => e.GrandChildCode);
            builder.Property(e => e.AccountsPayableEmail);
            builder.Property(e => e.SystemTierDiscount);
            builder.Property(e => e.UploadTierDiscount);
            builder.Property(e => e.ForcastTierDiscount);
            builder.Property(e => e.WeekForcasted);
            builder.Property(e => e.ActualSpendForcastedWeeks);
            builder.Property(e => e.DropNotificationFlag);
            builder.Property(e => e.OkToReduce);
            builder.Property(e => e.DropNotificationDate);
            builder.Property(e => e.ActualSpendYTD);
            builder.Property(e => e.PremierGPOId);
            builder.Property(e => e.PremierGLN);
            builder.Property(e => e.PremierDirectPRNGPO);
            builder.Property(e => e.PremierDirectPRNGLN);
            builder.Property(e => e.PremierDirectPRNName);
            builder.Property(e => e.PremierTopParentGPO);
            builder.Property(e => e.PremierTopParentGLN);
            builder.Property(e => e.PremierTopParentName);
            builder.Property(e => e.PremierTopParentFlag);
            builder.Property(e => e.RegionalDirName);
            builder.Property(e => e.RegionalDirTitle);
            builder.Property(e => e.RegionalDirPhone);
            builder.Property(e => e.RegionalDirPhoneExt);
            builder.Property(e => e.RegionalDirEmail);
            builder.Property(e => e.PreviousGrandParent);
            builder.Property(e => e.PreviousParent);
            builder.Property(e => e.PreviousChild);
            builder.Property(e => e.ElderCode);
            builder.Property(e => e.NumberOfEmployees);
            builder.Property(e => e.NumberOfLocations);
            builder.Property(e => e.Revenue);
            builder.Property(e => e.DBDate);
            builder.Property(e => e.DBId);
            builder.Property(e => e.BusinessName);
            builder.Property(e => e.BusinessAddress1);
            builder.Property(e => e.BusinessAddress2);
            builder.Property(e => e.BusinessCity);
            builder.Property(e => e.BusinessState);
            builder.Property(e => e.BusinessZip);
            builder.Property(e => e.BusinessPhone);
            builder.Property(e => e.TradeStyleName);
            builder.Property(e => e.BusinessCEOName);
            builder.Property(e => e.BusinessCEOTitle);
            builder.Property(e => e.LineOfBusiness);
            builder.Property(e => e.TotalEmployees);
            builder.Property(e => e.EmployeesHere);
            builder.Property(e => e.ProgramName);
            builder.Property(e => e.LastChangeDate);
            builder.Property(e => e.LastChangeTime);
            builder.Property(e => e.ProgramUser);
            builder.Property(e => e.CollectionRep);
            builder.Property(e => e.Tier1Customer);
            builder.Property(e => e.SalesVolume);
            builder.Property(e => e.TableName);
            builder.Property(e => e.IsUpdated);
        }
    }
}
