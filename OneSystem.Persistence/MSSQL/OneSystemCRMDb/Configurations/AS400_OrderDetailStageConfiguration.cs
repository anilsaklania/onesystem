﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using OneSystem.Domain.CRM;
using System;
using System.Collections.Generic;
using System.Text;

namespace OneSystem.Persistence.MSSQL.OneSystemCRMDb.Configurations
{
    class AS400_OrderStageConfiguration : IEntityTypeConfiguration<AS400_OrderDetailStage>
    {
        public void Configure(EntityTypeBuilder<AS400_OrderDetailStage> builder)
        {
            builder.ToTable("AS400_OrderDetailStage");
            builder.HasKey(e => new { e.OrderNumber, e.LineNumber, e.EntryDate });
            builder.Property(e => e.From);
            builder.Property(e => e.To);
            builder.Property(e => e.EntryDate);
            builder.Property(e => e.Company);
            builder.Property(e => e.LineNumber);
            builder.Property(e => e.User);
            builder.Property(e => e.Employee);
            builder.Property(e => e.LastModifiedDate);
        }
    }
}
