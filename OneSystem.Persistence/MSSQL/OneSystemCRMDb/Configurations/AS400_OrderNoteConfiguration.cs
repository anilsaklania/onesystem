﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using OneSystem.Domain.CRM;
using System;
using System.Collections.Generic;
using System.Text;

namespace OneSystem.Persistence.MSSQL.OneSystemCRMDb.Configurations
{
    class AS400_OrderNoteConfiguration : IEntityTypeConfiguration<AS400_OrderNote>
    {
        public void Configure(EntityTypeBuilder<AS400_OrderNote> builder)
        {
            builder.ToTable("AS400_OrderNote");
            builder.HasKey(e => new { e.OrderNumber, e.NoteLineNumber });
            builder.Property(e => e.Company);
            builder.Property(e => e.EntryDate);
            builder.Property(e => e.User);
            builder.Property(e => e.Message);
            builder.Property(e => e.LastModifiedDate);
        }
    }
}
