﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using OneSystem.Domain.CRM;
using System;
using System.Collections.Generic;
using System.Text;

namespace OneSystem.Persistence.MSSQL.OneSystemCRMDb.Configurations
{
    class AS400_OrderDetailDestinationConfiguration : IEntityTypeConfiguration<AS400_OrderDetailDestination>
    {
        public void Configure(EntityTypeBuilder<AS400_OrderDetailDestination> builder)
        {
            builder.ToTable("AS400_OrderDetailDestination");
            builder.HasKey(e => new { e.OrderNumber, e.AddressNo});
            //builder.Property(e => e.CompanyCode);
            //builder.Property(e => e.OrderNumber).IsRequired();
            //builder.Property(e => e.AddressNo).IsRequired();
            builder.Property(e => e.LineNumber);
            builder.Property(e => e.CustomerName);
            builder.Property(e => e.Attention);
            builder.Property(e => e.Address1);
            builder.Property(e => e.Address2);
            builder.Property(e => e.City);
            builder.Property(e => e.State);
            builder.Property(e => e.Zip);
            builder.Property(e => e.Quantity);
            builder.Property(e => e.Residential);
            builder.Property(e => e.LastModifiedDate);
        }
    }
}
