﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using OneSystem.Domain.CRM;
using System;
using System.Collections.Generic;
using System.Text;

namespace OneSystem.Persistence.MSSQL.OneSystemCRMDb.Configurations
{
    public class SourceConfiguration : IEntityTypeConfiguration<Source>
    {
        public void Configure(EntityTypeBuilder<Source> builder)
        {
            builder.ToTable("Source");
            builder.HasKey(e => e.Id);
            builder.Property(e => e.Name).IsRequired();
            builder.Property(e => e.EndPoint);
            builder.Property(e => e.LastDataSourceSynced); 
            builder.Property(e => e.SyncEnabled);
            builder.Property(e => e.Timespan);
            builder.HasMany(e => e.DataSources)
                .WithOne(e => e.Src)
                .HasForeignKey(e => e.SourceId)
                .HasConstraintName("FK_DataSource_Source");
            builder.HasMany(e => e.DataSource_Attributes)
                .WithOne(e => e.Src)
                .HasForeignKey(e => e.SourceId)
                .HasConstraintName("FK_DataSource_Attribute_Source");
            //builder.HasMany(e => e.DataSourceAttributes)
            //    .WithOne(e => e.Src)
            //    .HasForeignKey(e => e.SourceId)
            //    .HasConstraintName("FK_DataSourceAttribute_DataSource");

        }
    }
}
