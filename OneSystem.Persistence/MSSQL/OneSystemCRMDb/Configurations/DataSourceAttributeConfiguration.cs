﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using OneSystem.Domain.CRM;
using System;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace OneSystem.Persistence.MSSQL.OneSystemCRMDb.Configurations
{
    public class DataSourceAttributeConfiguration : IEntityTypeConfiguration<DataSourceAttribute>
    {
        public void Configure(EntityTypeBuilder<DataSourceAttribute> builder)
        {
            builder.ToTable("DataSourceAttribute");
            builder.HasKey(x => new { x.AttributeId, x.DataSourceId });
            builder.Property(x => x.Value).HasMaxLength(4000);
        }
    }
}
