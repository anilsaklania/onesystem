﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using OneSystem.Domain.CRM;
using System;
using System.Collections.Generic;
using System.Text;

namespace OneSystem.Persistence.MSSQL.OneSystemCRMDb.Configurations
{
    public class AS400_CustomerConfiguration : IEntityTypeConfiguration<AS400_Customer>
    {
        public void Configure(EntityTypeBuilder<AS400_Customer> builder)
        {
            builder.ToTable("AS400_Customer");
            //builder.HasKey(e => e.Id);
            builder.HasKey(e => new { e.CustomerNo, e.CustomerSegmentNo } );
            //builder.HasKey(e => e.CustomerNo);
            //builder.Property(e => e.CustomerNo).IsRequired();
            builder.Property(e => e.CustomerSegmentNo).IsRequired();
            builder.Property(e => e.CustomerName);
            builder.Property(e => e.CustomerEmail);
            builder.Property(e => e.CompanyCode);
            builder.Property(e => e.SortKey);
            builder.Property(e => e.AddressLine1);
            builder.Property(e => e.AddressLine2);
            builder.Property(e => e.City);
            builder.Property(e => e.State);
            builder.Property(e => e.Zip);
            builder.Property(e => e.PhoneNumber);
            builder.Property(e => e.PhoneExt);
            builder.Property(e => e.ContactPerson);
            builder.Property(e => e.BusinessType);
            builder.Property(e => e.CreditOKFlag);
            builder.Property(e => e.MailingLabel);
            builder.Property(e => e.SubsetCode);
            builder.Property(e => e.CustomerType);
            builder.Property(e => e.SalesmanOld);
            builder.Property(e => e.FaxNo);
            builder.Property(e => e.TestCustomer);
            builder.Property(e => e.PrefCredit);
            builder.Property(e => e.ProspectCustomer);
            builder.Property(e => e.IncludeOverrun);
            builder.Property(e => e.CustomerPOReq);
            builder.Property(e => e.UsePaymtVoucher);
            builder.Property(e => e.ListSales);
            builder.Property(e => e.MailSolicit);
            builder.Property(e => e.TelSolicit);
            builder.Property(e => e.FaxSolicit);
            builder.Property(e => e.ContactTitle);
            builder.Property(e => e.DecisionMakerName);
            builder.Property(e => e.DecisionMakerTitle);
            builder.Property(e => e.FirstOrderProd);
            builder.Property(e => e.FirstOrderKey);
            builder.Property(e => e.CreditDate);
            builder.Property(e => e.FirstOrderDate);
            builder.Property(e => e.CreditLimit);
            builder.Property(e => e.CreationDate);
            builder.Property(e => e.OpeningBalance);
            builder.Property(e => e.LastPaymentDate);
            builder.Property(e => e.TaxRate1);
            builder.Property(e => e.LastOrderDate);
            builder.Property(e => e.TaxRate2);
            builder.Property(e => e.HighCreditAmount);
            builder.Property(e => e.OpenOrderAmount);
            builder.Property(e => e.AvgPaymentAgeCurrent);
            builder.Property(e => e.AvgPaymentAgeLastYear);
            builder.Property(e => e.LastOrderProd);
            builder.Property(e => e.LastOrderKey);
            builder.Property(e => e.TaxID);
            builder.Property(e => e.TaxJurisdtn1);
            builder.Property(e => e.TaxJurisdtn2);
            builder.Property(e => e.TaxExempt);
            builder.Property(e => e.ResaleCertified);
            builder.Property(e => e.FinanceCharge);
            builder.Property(e => e.CreditCardUsed);
            builder.Property(e => e.StandardTerms);
            builder.Property(e => e.ShipToSameAddress);
            builder.Property(e => e.AcceptPartialInvoice);
            builder.Property(e => e.Comments);
            builder.Property(e => e.Class);
            builder.Property(e => e.NewSalesPerson);
            builder.Property(e => e.CountryCode);
            builder.Property(e => e.InHouse);
            builder.Property(e => e.ARStatement);
            builder.Property(e => e.Status);
            builder.Property(e => e.CreditLimitUpdFlag);
            builder.Property(e => e.SpellingOK);
            builder.Property(e => e.EmailSolicit);
            builder.Property(e => e.SchoolType);
            builder.Property(e => e.MaintenanceDate);
            builder.Property(e => e.UploadDateToMailMgr);
            builder.Property(e => e.NoOfSampleKits);
            builder.Property(e => e.TableName);
            builder.Property(e => e.IsUpdated);
        }
    }
}
