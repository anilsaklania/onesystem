﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using OneSystem.Domain.CRM;
using System;
using System.Collections.Generic;
using System.Text;

namespace OneSystem.Persistence.MSSQL.OneSystemCRMDb.Configurations
{
    public class DataSourceConfiguration : IEntityTypeConfiguration<DataSource>
    {
        public void Configure(EntityTypeBuilder<DataSource> builder)
        {
            builder.ToTable("DataSource");
            builder.HasKey(x => x.Id);
            builder.Property(x => x.RefId);
            builder.Property(x => x.LastModifiedDate);
            builder.Property(x => x.IsCheckedInAPI);
            builder.HasMany(e => e.DataSourceAttribute)
                .WithOne(e => e.DataSource)
                .HasForeignKey(e => e.DataSourceId)
                .HasConstraintName("FK_DataSourceAttribute_DataSource");
        }
    }
}
