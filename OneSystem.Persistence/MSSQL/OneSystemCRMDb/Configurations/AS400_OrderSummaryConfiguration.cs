﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using OneSystem.Domain.CRM;
using System;
using System.Collections.Generic;
using System.Text;

namespace OneSystem.Persistence.MSSQL.OneSystemCRMDb.Configurations
{
    class AS400_OrderSummaryConfiguration : IEntityTypeConfiguration<AS400_OrderSummary>
    {
        public void Configure(EntityTypeBuilder<AS400_OrderSummary> builder)
        {
            builder.ToTable("AS400_OrderSummary");
            builder.HasKey(e => e.OrderNumber);
            builder.Property(e => e.CompanyCode);
            builder.Property(e => e.CampaignCode);
            builder.Property(e => e.CustomerNumber);
            builder.Property(e => e.EntryDate);
            builder.Property(e => e.ExtraChargeAmount);
            builder.Property(e => e.OrderQuantityHashTotal);
            builder.Property(e => e.Item);
            builder.Property(e => e.TotalMerchandiseAmount);
            builder.Property(e => e.OrderType);
            builder.Property(e => e.CustomerPONumber);
            builder.Property(e => e.FirstThreeCharOfZip);
            builder.Property(e => e.StreetNumber);
            builder.Property(e => e.CustomerSegment);
            builder.Property(e => e.TotalOfMdseSetupExtraShippingTax);
            builder.Property(e => e.SetupChargeAmount);
            builder.Property(e => e.ShippingChargeAmount);
            builder.Property(e => e.TaxAmount);
            builder.Property(e => e.LastModifiedDate);
        }
    }
}
