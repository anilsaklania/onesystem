﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using OneSystem.Domain.CRM;
using System;
using System.Collections.Generic;
using System.Text;

namespace OneSystem.Persistence.MSSQL.OneSystemCRMDb.Configurations
{
    public class LeadSourceMetadataConfiguration : IEntityTypeConfiguration<LeadSourceMetadata>
    {
        public void Configure(EntityTypeBuilder<LeadSourceMetadata> builder)
        {
            builder.ToTable("LeadSourceMetadata");
            builder.HasKey(e => e.Id);
            builder.Property(e => e.EntryDate).IsRequired();
            builder.Property(e => e.Name).IsRequired();
            builder.Property(e => e.Value).HasMaxLength(500);
            
        }
    }
}
