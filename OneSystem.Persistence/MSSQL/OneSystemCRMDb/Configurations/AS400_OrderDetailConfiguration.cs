﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using OneSystem.Domain.CRM;
using System;
using System.Collections.Generic;
using System.Text;

namespace OneSystem.Persistence.MSSQL.OneSystemCRMDb.Configurations
{
    class AS400_OrderDetailConfiguration : IEntityTypeConfiguration<AS400_OrderDetail>
    {
        public void Configure(EntityTypeBuilder<AS400_OrderDetail> builder)
        {
            builder.ToTable("AS400_OrderDetail");
            builder.HasKey(e => new { e.OrderNumber, e.LineNumber });
            builder.Property(e => e.CompanyCode);
            builder.Property(e => e.LineNumber).IsRequired();
            builder.Property(e => e.Item);
            builder.Property(e => e.BillToCustomer);
            builder.Property(e => e.BillToSegment);
            builder.Property(e => e.ShipToCustomer);
            builder.Property(e => e.ShipToSegment);
            builder.Property(e => e.OrderCount);
            builder.Property(e => e.OrderedQuantity);
            builder.Property(e => e.FreeQuantity);
            builder.Property(e => e.OverrunQuantity);
            builder.Property(e => e.ShippedQuantity);
            builder.Property(e => e.UnitPrice);
            builder.Property(e => e.LinePrice);
            builder.Property(e => e.AdjustmentReasonCode);
            builder.Property(e => e.TotalPrice);
            builder.Property(e => e.PrepaidAmount);
            builder.Property(e => e.DropShipFlag);
            builder.Property(e => e.SetupCharge);
            builder.Property(e => e.ExtraCharge);
            builder.Property(e => e.ShippingCharge);
            builder.Property(e => e.HandlingCharge);
            builder.Property(e => e.Tax1);
            builder.Property(e => e.Tax2);
            builder.Property(e => e.Status);
            builder.Property(e => e.InvoiceNumber);
            builder.Property(e => e.VendorId);
            builder.Property(e => e.OurPONumber);
            builder.Property(e => e.DueDate);
            builder.Property(e => e.ShipDate);
            builder.Property(e => e.ShippingCarrier);
            builder.Property(e => e.ImprintSuffix);
            builder.Property(e => e.SampleOrder);
            builder.Property(e => e.Unknown);
            builder.Property(e => e.InternalShipCode);
            builder.Property(e => e.ProductionBatchNumber);
            builder.Property(e => e.MfgBatchLine);
            builder.Property(e => e.UnitCost);
            builder.Property(e => e.TypeSetterInitial); 
            builder.Property(e => e.TypesetDate); 
            builder.Property(e => e.ProductionInitial);
            builder.Property(e => e.ProductionDate);
            builder.Property(e => e.PackagingInitial);
            builder.Property(e => e.PackedDate);
            builder.Property(e => e.ShippingInitial);
            builder.Property(e => e.InvoiceStat);
            builder.Property(e => e.AcknoFlag);
            builder.Property(e => e.ListPrice);
            builder.Property(e => e.DiscCode);
            builder.Property(e => e.InvoiceDate);
            builder.Property(e => e.TypeSetTime);
            builder.Property(e => e.ProdTime);
            builder.Property(e => e.PackTime);
            builder.Property(e => e.ShippedTime);
            builder.Property(e => e.EstimatedShippingCharge);
            builder.Property(e => e.EstimatedHandlingCharge);
            builder.Property(e => e.LastShipmentQuantity);
            builder.Property(e => e.CustomerOrPositivePays);
            builder.Property(e => e.BillingMethod);
            builder.Property(e => e.PickerNo);
            builder.Property(e => e.InventoryUpdated);
            builder.Property(e => e.ConvertedPO_OrderMemoPrinted_ShopOrder);
            builder.Property(e => e.SubstitutionDone);
            builder.Property(e => e.UPSGrndDaysinTransit);
            builder.Property(e => e.InHandDate);
            builder.Property(e => e.StandrdExpressOrRush);
            builder.Property(e => e.GiftaDayItemSequence);
            builder.Property(e => e.ResidentialBldg);
            builder.Property(e => e.OriginalShippingMethod);
            builder.Property(e => e.GiftaDayItem);
            builder.Property(e => e.AllocatedOrNot);
            builder.Property(e => e.Filler);
            builder.Property(e => e.LastModifiedDate);

            //builder.HasMany(e => e.Destinations)
            //     .WithOne(e => e.OrderDetail)
            //     .HasForeignKey(e => new { e.OrderNumber, e.LineNumber })
            //     .HasConstraintName("FK_AS400OrderDetailDestination_AS400OrderDetail");
            //builder.HasMany(e => e.Stages)
            //    .WithOne(e => e.Detail)
            //    .HasForeignKey(e => new { e.OrderNumber, e.LineNumber })
            //    .HasConstraintName("FK_AS400OrderStage_AS400OrderDetail");
        }
    }
}
