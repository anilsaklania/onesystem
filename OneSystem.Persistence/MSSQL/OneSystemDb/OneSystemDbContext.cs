﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using OneSystem.Domain.Entities;

namespace OneSystem.Persistence.Entities
{
    public partial class OneSystemDbContext : DbContext
    {
        public OneSystemDbContext()
        {
        }

        public OneSystemDbContext(DbContextOptions<OneSystemDbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<ProductAttribute> ProductAttributes { get; set; } = null!;
        public virtual DbSet<ProductColor> ProductColors { get; set; } = null!;
        public virtual DbSet<ProductProductAttributeAudit> ProductProductAttributeAudits { get; set; } = null!;

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("server=pos-web-server2.positive.local;user=svam;password=svam@pos;database=OneSystemDb");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ProductAttribute>(entity =>
            {
                entity.ToTable("Product_Attribute");

                entity.HasIndex(e => e.AuditId, "IX_Product_Attribute_AuditId");

                entity.HasIndex(e => e.ColorId, "IX_Product_Attribute_ColorId");

                entity.HasIndex(e => e.Name, "IX_Product_Attribute_Name");

                entity.HasIndex(e => new { e.Type, e.ParentAttributeId, e.Name }, "IX_Product_Attribute_NameParentType");

                entity.HasIndex(e => e.ParentAttributeId, "IX_Product_Attribute_ParentAttributeId");

                entity.HasIndex(e => e.Type, "IX_Product_Attribute_Type");

                entity.HasIndex(e => e.Value, "IX_Product_Attribute_Value");

                entity.Property(e => e.EntryDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Hiearchy).HasMaxLength(4000);

                entity.Property(e => e.HiearchyString).HasMaxLength(4000);

                entity.Property(e => e.LastModifiedDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.LastUsername).HasMaxLength(50);

                entity.Property(e => e.Name).HasMaxLength(250);

                entity.Property(e => e.Type).HasMaxLength(250);

                entity.Property(e => e.Username).HasMaxLength(50);

                entity.Property(e => e.Value).HasMaxLength(1000);

                entity.HasOne(d => d.Audit)
                    .WithMany(p => p.ProductAttributes)
                    .HasForeignKey(d => d.AuditId)
                    .HasConstraintName("FK_Product_Attribute_ProductAttributeAudit");

                entity.HasOne(d => d.Color)
                    .WithMany(p => p.ProductAttributes)
                    .HasForeignKey(d => d.ColorId)
                    .HasConstraintName("FK_Product_Attribute_ProductColor");

                entity.HasOne(d => d.ParentAttribute)
                    .WithMany(p => p.InverseParentAttribute)
                    .HasForeignKey(d => d.ParentAttributeId)
                    .HasConstraintName("FK_Product_Attribute_Product_Attribute");
            });

            modelBuilder.Entity<ProductColor>(entity =>
            {
                entity.ToTable("Product_Color");

                entity.HasIndex(e => new { e.Pms, e.Hex }, "IX_Product_Color_PMSHex")
                    .IsUnique();

                entity.Property(e => e.Hex).HasMaxLength(250);

                entity.Property(e => e.IsActive)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.LastModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.Name).HasMaxLength(250);

                entity.Property(e => e.Pms)
                    .HasMaxLength(50)
                    .HasColumnName("PMS");

                entity.Property(e => e.SwatchId).HasMaxLength(250);
            });

            modelBuilder.Entity<ProductProductAttributeAudit>(entity =>
            {
                entity.ToTable("Product_ProductAttribute_Audit");

                entity.HasIndex(e => e.Type, "IX_Product_ProductAttribute_Audit_TypeValue");

                entity.Property(e => e.DataType).HasMaxLength(50);

                entity.Property(e => e.EntryDate).HasColumnType("datetime");

                entity.Property(e => e.LastValidationDate).HasColumnType("datetime");

                entity.Property(e => e.Type).HasMaxLength(50);

                entity.Property(e => e.Value).HasMaxLength(2000);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
